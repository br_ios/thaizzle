//
//  Inspectable.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 06/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import  UIKit
class BackGroundView: UIView {
    
    @IBDesignable
    class DesignableView: UIView {
    }
    
    @IBDesignable
    class DesignableButton: UIButton {
    }
    
    @IBDesignable
    class DesignableLabel: UILabel {
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.*/
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUp()
    }
    func setUp()   {
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 10
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 0.7
        
    }
    
}


