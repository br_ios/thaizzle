//
//  DefaultHelper.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 01/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import  UIKit

private enum Defaults: String {
    case addLang = "addLang"
}



class DefaultsHelper {
    
    static func setLanguage(withCheck Check: String) {
        UserDefaults.standard.set(Check, forKey: Defaults.addLang.rawValue)
    }
    
    static func getLanguage()-> String?{
        return UserDefaults.standard.string(forKey: Defaults.addLang.rawValue)
    }
    
    static func deleteLanguage() {
        UserDefaults.standard.removeObject(forKey: Defaults.addLang.rawValue)
 }
}
