//
//  BusinessDetailController.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 07/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import NVActivityIndicatorView
import Firebase
import FirebaseMessaging
import UserNotifications
import FirebaseCore
import FirebaseInstanceID
import GoogleMobileAds

class BusinessDetailController: UIViewController,NVActivityIndicatorViewable {
    
    var id = String()
    var businessTitle = ""
    var addListingArrayModel = [AdListing]()
    var obj_listingData : DataDetail? = nil
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelTwo: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var labelFour: UILabel!
    @IBOutlet weak var lbl_postedByText: UILabel!

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showLoader()
        ad_forest_BusinessData()
        showBackButton()
    }
    
    
    @IBOutlet var controllerVie: UIView!{
        didSet{
            controllerVie.addShadowToView()

        }
    }
    
    func ad_forest_BusinessData() {
        
        AddsHandler.businessDetailData(author_id: id , success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                self.title = successResponse.data.text
                self.businessTitle = successResponse.message
                AddsHandler.sharedInstance.objBusinessModelRoot = successResponse.data
                self.obj_listingData = AddsHandler.sharedInstance.objBusinessModelRoot?.dataA
                self.initialSetUp()
                self.addListingArrayModel = AddsHandler.sharedInstance.objBusinessModelRoot?.ad_listing ?? []
                if self.addListingArrayModel.count != 0{
                self.tableView.reloadData()
                }
            } else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            self.stopAnimating()
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    
    func initialSetUp(){
        self.labelOne.text = self.obj_listingData?.display_name ?? ""
        self.labelTwo.text = self.obj_listingData?.contact ?? ""
        self.loginLabel.text = self.obj_listingData?.last_login ?? ""
        self.labelFour.text = self.obj_listingData?.user_type ?? ""
        self.lbl_postedByText.text = self.obj_listingData?.posted_by_text ?? ""
   
        if   let url_ProductImg = URL(string: self.obj_listingData?.user_image_url ?? ""){
            self.productImage.sd_setShowActivityIndicatorView(true)
            self.productImage.sd_setIndicatorStyle(.gray)
            self.productImage.sd_setImage(with: url_ProductImg, completed: nil)
        }
    }
    
    func showLoader(){
        self.startAnimating(Constants.activitySize.size, message: Constants.loaderMessages.loadingMessage.rawValue,messageFont: UIFont.systemFont(ofSize: 14), type: NVActivityIndicatorType.ballClipRotatePulse)
    }
}

extension BusinessDetailController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addListingArrayModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : BusinessDetailTableCell = tableView.dequeueReusableCell(withIdentifier: "BusinessDetailTableCell", for: indexPath) as! BusinessDetailTableCell
        let obj = addListingArrayModel[indexPath.row]
        cell.productNameLbl.text = obj.ad_title ?? ""
        cell.productTypeLbl.text = obj.cat_name ?? ""
        cell.labelThree.text = obj.ad_content ?? ""
        let obj_location:LocationData = obj.location
        cell.locationLbl.text = obj_location.title ?? ""
        cell.postedDateLbl.text = obj.ad_date ?? ""
        let arr_images:[ProductImageModel] = obj.images
        let str_imageUrl = arr_images.first
        
        if   let url_ProductImg = URL(string: str_imageUrl?.thumb ?? ""){
            cell.productImage.sd_setShowActivityIndicatorView(true)
            cell.productImage.sd_setIndicatorStyle(.gray)
            cell.productImage.sd_setImage(with: url_ProductImg, completed: nil)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = addListingArrayModel[indexPath.row]
        let detailAdVC = self.storyboard?.instantiateViewController(withIdentifier: "AddDetailController") as! AddDetailController
        detailAdVC.ad_id = obj.ad_id ?? 0
        self.navigationController?.pushViewController(detailAdVC, animated: true)
    }
}
