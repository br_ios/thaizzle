//
//  BusinessDetailTableCell.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 07/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class BusinessDetailTableCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productTypeLbl: UILabel!
    @IBOutlet weak var labelThree: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var postedDateLbl: UILabel!
    
    @IBOutlet var productListView: UIView!{
        didSet{
            productListView.addShadowToView()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
