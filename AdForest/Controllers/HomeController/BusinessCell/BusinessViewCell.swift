//
//  BusinessViewCell.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 06/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

protocol BusinessDelegate {
    func businessDetail(id: String)
}

class BusinessViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var businessData = [BusinessShowRoom]()
     var btnViewAllBusiness: (()->())?
     var delegate : BusinessDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    

    @IBOutlet var titleImage: UIImageView!
    
    @IBAction func actionShowAllBusiness(_ sender: UIButton) {
        self.btnViewAllBusiness?()
    }
    
    @IBOutlet weak var btnViewAll: UIButton!{
        didSet {
            btnViewAll.roundCornors(radius: 5)
            if let mainColor = UserDefaults.standard.string(forKey: "mainColor") {
                btnViewAll.backgroundColor = Constants.hexStringToUIColor(hex: mainColor)
            }
        }
        
    }
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var containerView: UIView!{
    didSet{
       self.containerView.backgroundColor = .clear
      }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    
  
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return businessData.count
    }
    
  
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : BusinessCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BusinessCollectionViewCell", for: indexPath) as! BusinessCollectionViewCell
        let objData = businessData[indexPath.row]
        cell.labelOne.text = objData.user_nicename
        cell.labelTwo.text = objData.user_display_name
        if let imgUrl = URL(string: objData.user_image_url) {
            cell.image.sd_showActivityIndicatorView()
            cell.image.sd_setIndicatorStyle(.gray)
            cell.image.sd_setImage(with: imgUrl, completed: nil)
            cell.btnFullAction = { () in
                self.delegate?.businessDetail(id: objData.user_id)
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 140, height: 140)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
 
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView.isDragging {
            cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.3, animations: {
                cell.transform = CGAffineTransform.identity
            })
        }
    }
}

