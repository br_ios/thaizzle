//
//  BusinessCollectionViewCell.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 06/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class BusinessCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelTwo: UILabel!
    
     
    @IBOutlet weak var containerCellView: UIView!{
        didSet {
            containerCellView.addShadowToView()
        }
    }
   
    @IBOutlet weak var smallView: UIView!{
        didSet{
            smallView.addShadowToView()
            smallView.layer.cornerRadius = 10
        }
    }
    @IBOutlet weak var imageContainer: UIView!{
        didSet{
            imageContainer.addShadowToView()
            imageContainer.layer.cornerRadius = 10
        }
    }
    var btnFullAction: (()->())?
    
  @IBAction func actionFullBtn(_ sender: Any) {
     self.btnFullAction?()
    
    }
}
