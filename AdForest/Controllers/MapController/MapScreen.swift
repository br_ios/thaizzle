//
//  MapScreen.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 09/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import CoreLocation
import GooglePlaces

protocol mapCartCordinateDelegate {
    func mapLatLong(lat: String, long: String , address: String)
}

class MapScreen: UIViewController, CLLocationManagerDelegate , UISearchControllerDelegate, UISearchDisplayDelegate, GMSMapViewDelegate {
    
     @IBOutlet var mapView: GMSMapView!
    
    let searchBarNavigation = UISearchBar()
    var isNavSearchBarShowing = false
    var navigationView = UIView()
    
    var delegate: mapCartCordinateDelegate?
    let appDel = UIApplication.shared.delegate as! AppDelegate
    let locationManager = CLLocationManager()
    var locationValue = CLLocationCoordinate2D()
    var searchBar: UISearchBar?
    var markerBtn: UIButton?
    var tableDataSource: GMSAutocompleteTableDataSource?
    var searchController: UISearchDisplayController?
    
    override func viewDidLoad() {
       // super.viewDidLoad()
        setUpLocationManager()
        let y = UIApplication.shared.statusBarFrame.size.height + 5
        searchBar = UISearchBar(frame: CGRect(x: 0, y: y, width: UIScreen.main.bounds.width, height: 44.0))
      
        tableDataSource = GMSAutocompleteTableDataSource()
        tableDataSource?.delegate = self
        searchController = UISearchDisplayController(searchBar: searchBar!, contentsController: self)
        searchController?.searchResultsDataSource = tableDataSource
        searchController?.searchResultsDelegate = tableDataSource
        searchController?.delegate = self
        navigationButtons()
    }
    
    @objc func handleTap(_ sender: UIButton) {
        print("You tapped a button")
        let lat : String = self.locationValue.latitude.description
        let lng : String = self.locationValue.longitude.description
        self.findCurrentLocationAddress(location: self.locationValue) { (address) in
            self.delegate?.mapLatLong(lat: lat, long: lng , address: address )
        }
       self.appDel.dissmissController()
    }

    func navigationButtons() {
        //Search Button
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(UIImage(named: "search"), for: .normal)
        if #available(iOS 11, *) {
            searchBarNavigation.widthAnchor.constraint(equalToConstant: 30).isActive = true
            searchBarNavigation.heightAnchor.constraint(equalToConstant: 30).isActive = true
        } else {
            searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        }
        searchButton.addTarget(self, action: #selector(actionSearch), for: .touchUpInside)
        let searchItem = UIBarButtonItem(customView: searchButton)
        self.navigationItem.rightBarButtonItem = searchItem
    }

    //MARK:- Search Controller
    @objc func actionSearch(_ sender: Any) {
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        print(marker.position.latitude)
        print(marker.position.longitude)
    }
    
    func findCurrentLocationAddress( location : CLLocationCoordinate2D , responseClosure : @escaping ( _ address : String)->()) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(location) { response, error in
            if let address = response?.firstResult() {
                let lines = address.lines as [String]?
                responseClosure(lines!.joined(separator: ","))
            }
        }
    }
    
    func didUpdateAutocompletePredictionsForTableDataSource(tableDataSource: GMSAutocompleteTableDataSource) {
        // Turn the network activity indicator off.
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        // Reload table data.
        searchController?.searchResultsTableView.reloadData()
    }
    
    func didUpdateAutocompletePredictions(for tableDataSource: GMSAutocompleteTableDataSource) {
        // Turn the network activity indicator off.
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        // Reload table data.
        searchController?.searchResultsTableView.reloadData()
    }

    
//    @IBAction func searchButton(_ sender: UIButton) {
//    }
    
    
    func CameraPosition(){
        let y = UIApplication.shared.statusBarFrame.size.height + 55
       let camera = GMSCameraPosition(target: self.locationValue, zoom: 15 , bearing: 0, viewingAngle: 0)
        mapView = GMSMapView(frame: CGRect(x: 0, y: y, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), camera: camera)
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.delegate = self
        self.view.insertSubview(mapView, at: 0)
        self.view.addSubview(mapView)
      
        
        let button = UIButton(frame: CGRect(x: (UIScreen.main.bounds.width - 250) / 2 , y: (UIScreen.main.bounds.height - 120), width: 250, height: 40))
        button.backgroundColor = UIColor.lightGray
        button.setTitle("Confirm", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(handleTap), for: .touchUpInside)
        
        self.view.insertSubview(button, at: 0)
        view.addSubview(button)
        
        self.markerBtn = UIButton(frame: CGRect(x: (UIScreen.main.bounds.size.width - 40) / 2, y: (UIScreen.main.bounds.size.height - 40) / 2, width: 40, height: 40))
        self.markerBtn?.setImage(UIImage(named: "marker"), for: .normal)
        self.view.insertSubview(markerBtn!, at: 0)
        view.addSubview(markerBtn!)
        
        
       self.navigationView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: y))
        navigationView.backgroundColor = UIColor.orange
        
        self.view.insertSubview(navigationView, at: 0)
        view.addSubview(navigationView)
        for subView in searchBar!.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    let attributes = [NSAttributedStringKey.foregroundColor: UIColor.black] as [NSAttributedStringKey: Any]
                    textField.attributedPlaceholder =  NSAttributedString(string:NSLocalizedString("Search", comment:""),
                                                                          attributes: attributes)
                    
                    textField.leftViewMode = UITextFieldViewMode.never
                    textField.font = UIFont.systemFont(ofSize: 16.0)
                    textField.backgroundColor = UIColor.white
                    textField.returnKeyType = UIReturnKeyType.done
                }
            }
        }
        searchBar?.tintColor = UIColor.white
        searchBar?.backgroundImage = UIImage()
        navigationView.insertSubview(searchBar!, at: 0)
        navigationView.addSubview(searchBar!)
        
    }
    
    func setUpLocationManager () {
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationValue = (locationManager.location?.coordinate)!
        locationManager.stopUpdatingLocation()
        self.CameraPosition()
      
        
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.locationValue = position.target
        
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        self.locationValue = position.target
        
    }
}
extension MapScreen: GMSAutocompleteTableDataSourceDelegate {
    func tableDataSource(_ tableDataSource: GMSAutocompleteTableDataSource, didFailAutocompleteWithError error: Error) {
         print("Error: \(error.localizedDescription)")
    }

    func tableDataSource(_ tableDataSource: GMSAutocompleteTableDataSource, didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        let lat : String = place.coordinate.latitude.description
        let lng : String = place.coordinate.longitude.description
        self.delegate?.mapLatLong(lat: lat, long: lng , address: place.formattedAddress ?? "")
        self.appDel.dissmissController()
    }

    func searchDisplayController(_ controller: UISearchDisplayController, shouldReloadTableForSearch searchString: String?) -> Bool {
        tableDataSource?.sourceTextHasChanged(searchString)
        return false
    }

    func tableDataSource(_ tableDataSource: GMSAutocompleteTableDataSource, didSelect prediction: GMSAutocompletePrediction) -> Bool {
        return true
    }
}


