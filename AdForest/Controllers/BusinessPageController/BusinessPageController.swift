//
//  BusinessPageController.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 08/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BusinessPageController: UIViewController,NVActivityIndicatorViewable {
    @IBOutlet weak var tableView: UITableView!
    
    var page = Int()
    var arr_businessShowroomListing = [UserData]()
    var canEdit = false
    var refreshBool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        page = 1
        //remove array when view start loading
        self.arr_businessShowroomListing.removeAll()
        self.tableView.reloadData()
        self.showLoader()
        getBusinessListingApi()
        showBackButton()
    }
    
    
    
    func getBusinessListingApi() {
        AddsHandler.businessShowroomListing(page: page, success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                self.title = successResponse.dataIs.text
                let data_showroomListing:BusinessPageDetail = successResponse.dataIs
                let userDetail:[UserData] = data_showroomListing.user
                if userDetail.count != 0{
                    //self.arr_businessShowroomListing = userDetail
                    for dict in userDetail{
                        self.arr_businessShowroomListing.append(dict)
                    }
                     self.canEdit = true
                    self.tableView.reloadData()
                }
            } else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            self.stopAnimating()
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    func showLoader(){
        self.startAnimating(Constants.activitySize.size, message: Constants.loaderMessages.loadingMessage.rawValue,messageFont: UIFont.systemFont(ofSize: 14), type: NVActivityIndicatorType.ballClipRotatePulse)
    }
}

extension BusinessPageController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_businessShowroomListing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : BusinessPageCell = tableView.dequeueReusableCell(withIdentifier: "BusinessPageCell", for: indexPath) as! BusinessPageCell
        let obj = arr_businessShowroomListing[indexPath.row]
        cell.labelOne.text = obj.user_login ?? ""
        cell.labelTwo.text = obj.user_email ?? ""
        
        if  let url_ProductImg = URL(string: obj.user_image_url ?? ""){
            cell.productImage.sd_setShowActivityIndicatorView(true)
            cell.productImage.sd_setIndicatorStyle(.gray)
            cell.productImage.sd_setImage(with: url_ProductImg, completed: nil)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arr_businessShowroomListing[indexPath.row]
        let businessVC = self.storyboard?.instantiateViewController(withIdentifier: "BusinessDetailController") as! BusinessDetailController
        businessVC.id = obj.user_id ?? ""
        self.navigationController?.pushViewController(businessVC, animated: true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y;
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        
        if (maximumOffset - currentOffset <= -40.0 && canEdit) {
            addEventsAndRefresh()
        }
    }
    
    func addEventsAndRefresh() {
        if (canEdit){
            refreshBool = true
            canEdit = false
            page += 1
            getBusinessListingApi()
        }
    }
}

