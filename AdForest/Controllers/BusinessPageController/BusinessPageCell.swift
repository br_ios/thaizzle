//
//  BusinessPageCell.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 08/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class BusinessPageCell: UITableViewCell {

    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelTwo: UILabel!
    @IBOutlet weak var productImage: UIImageView!

    @IBOutlet var productListView: UIView!{
        didSet{
            productListView.addShadowToView()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
