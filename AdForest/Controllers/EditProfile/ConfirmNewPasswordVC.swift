//
//  ConfirmNewPasswordVC.swift
//  AdForest
//
//  Created by Admin 5 on 09/07/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ConfirmNewPasswordVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var txtfldNewPassword: UITextField!
    
    @IBOutlet weak var txtfldVerifyPassword: UITextField!
    
    var ConfirmUserId = String()
    override func viewDidLoad() {
        super.viewDidLoad()
       
        txtfldNewPassword.delegate = self
        txtfldVerifyPassword.delegate = self
        
        print(ConfirmUserId)
    }
    
    
    func Changepassword(password1:String,password2:String,userId:String){
        
        
        UserHandler.Changepassword(password1: txtfldNewPassword.text!, password2: txtfldVerifyPassword.text!, userId: ConfirmUserId, success: { (successResponse) in
            if successResponse.success != true {
               
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            
              let alert = Constants.showBasicAlert(message: error.message)
              self.presentVC(alert)
        }
    }
    @IBAction func actionChangePassword(_ sender: Any) {
        if txtfldNewPassword.text == "" {
            
            let alertController = UIAlertController(title: "Error", message: "Please enter password", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            
        }else if txtfldVerifyPassword.text == "" {
            
            let alertController = UIAlertController(title: "Error", message: "Please enter confirm password", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            
//        }else if !txtfldNewPassword.text!.isValidPassword()
//        {
//            let alertController = UIAlertController(title: "Error", message: "Passwords does not Match", preferredStyle: .alert)
//            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//            alertController.addAction(defaultAction)
//            self.present(alertController, animated: true, completion: nil)
//            
////        }else if !txtfldNewPassword.text!.containsSpecialCharacter
////        {
////
////            let alertController = UIAlertController(title: "Error", message: "Please input at least one special character in password.", preferredStyle: .alert)
////            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
////            alertController.addAction(defaultAction)
////            self.present(alertController, animated: true, completion: nil)
////
////        }
        } else if txtfldNewPassword.text != txtfldVerifyPassword.text
        {
            let alertController = UIAlertController(title: "Error", message: "Passwords don't Match", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            
        }else{
            Changepassword(password1:txtfldNewPassword.text!,password2:txtfldVerifyPassword.text!, userId: ConfirmUserId)
     
        }
}
}
extension String
{
    public func isValidPassword() -> Bool {
        let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{7,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
    
    var containsSpecialCharacter: Bool {
        let regex = ".*[^A-Za-z0-9].*"
        let testString = NSPredicate(format:"SELF MATCHES %@", regex)
        return testString.evaluate(with: self)
    }
}
