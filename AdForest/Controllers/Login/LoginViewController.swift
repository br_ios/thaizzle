//
//  LoginViewController.swift
//  Adforest
//
//  Created by apple on 1/2/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import NVActivityIndicatorView
import SDWebImage
import UITextField_Shake
import LineSDK
import SKCountryPicker


var userId = Int()
class LoginViewController: UIViewController, UITextFieldDelegate, NVActivityIndicatorViewable, GIDSignInUIDelegate, GIDSignInDelegate, UIScrollViewDelegate{
    
    //MARK:- Outlets
  
    @IBOutlet var countryCode: UITextField!{
        didSet {
            countryCode.delegate = self
        }
    }
    @IBOutlet weak var containerViewImage: UIView!
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var imgEmail: UIImageView!
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var txtEmail: UITextField! {
        didSet {
            txtEmail.delegate = self
        }
    }
   
    @IBOutlet weak var txtPassword: UITextField! {
        didSet {
            txtPassword.delegate = self
        }
    }
  
    
    
    @IBOutlet weak var viewRegisterWithUs: UIView!
    @IBOutlet weak var containerViewSocialButton: UIView!
    
    @IBAction func actionBackButton(_ sender: UIButton) {
   self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet var oltSubmitBtn: UIButton!
    @IBOutlet var oltForgotPswrd: UIButton!
    @IBOutlet var oltRegWithUs: UIButton!
    @IBOutlet var oltMobNo: UILabel!
    @IBOutlet var oltPassword: UILabel!
    @IBOutlet var oltConnectOtherWays: UILabel!
    
    
    
    //MARK:- Properties
    var getLoginDetails = [LoginData]()
    var defaults = UserDefaults.standard
    var isVerifyOn = false
    var isFromLeftMenu = false
    var selectedPhonenumber = ""
    let contryPickerController = CountryPickerController()
    
    // MARK: Application Life Cycle
    override func viewDidLoad() {
         super.viewDidLoad()
         self.hideKeyboard()
         GIDSignIn.sharedInstance().uiDelegate = self
         GIDSignIn.sharedInstance().delegate = self
         self.adForest_loginDetails()
        btnCountry.setTitle("+66", for: .normal)
       //  buttonGuestLogin.isHidden = false
      //  self.countryCode.text = "+66"
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        defaults.removeObject(forKey: "isGuest")
        defaults.synchronize()
       

       if isFromLeftMenu{
            oltBackBtn.setImage(UIImage(named: "menuLogin"), for: .normal)
       }
        else {
         oltBackBtn.setImage(UIImage(named: "backbuttonLogin"), for: .normal)
       }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBOutlet var oltBackBtn: UIButton!
    
    @IBAction func actnBackBtn(_ sender: UIButton) {
        if isFromLeftMenu{
             slideMenuController()?.toggleLeft()
        }
        else {
             navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func actionCountryCode(_ sender: Any) {
        let countryController = (CountryPickerWithSectionViewController).presentController(on: self) { (country: Country) in
            //            self.CountryFlagimg.image = country.flag
            self.btnCountry.setTitle(country.dialingCode, for: .normal)
            //
        }
    }
   
    
    //MARK:- text Field Delegate Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField == txtEmail {
            txtPassword.becomeFirstResponder()
        }
            else if textField == txtPassword {
            txtPassword.resignFirstResponder()
            self.adForest_logIn()
        }
        return true
    }
    
    //MARK: - Custom
    func showLoader() {
        self.startAnimating(Constants.activitySize.size, message: Constants.loaderMessages.loadingMessage.rawValue,messageFont: UIFont.systemFont(ofSize: 14), type: NVActivityIndicatorType.ballClipRotatePulse)
    }
    
    func adForest_populateData() {
        if UserHandler.sharedInstance.objLoginDetails != nil {
            let objData = UserHandler.sharedInstance.objLoginDetails
           
            if let isVerification = objData?.isVerifyOn {
                self.isVerifyOn = isVerification
            }
            
            if let bgColor = defaults.string(forKey: "mainColor") {
                //self.containerViewImage.backgroundColor = Constants.hexStringToUIColor(hex: bgColor)
                //self.buttonSubmit.layer.borderColor = Constants.hexStringToUIColor(hex: bgColor).cgColor
                 //self.buttonSubmit.backgroundColor = Constants.hexStringToUIColor(hex: bgColor)
                //self.buttonGuestLogin.layer.borderColor = Constants.hexStringToUIColor(hex: bgColor).cgColor
               // self.buttonSubmit.setTitleColor(Constants.hexStringToUIColor(hex: "ffffff"), for: .normal)
                
                //self.buttonGuestLogin.setTitleColor(Constants.hexStringToUIColor(hex: bgColor), for: .normal)
            }
            
//            if let imgUrl = URL(string: (objData?.logo)!) {
//                imgTitle.sd_setShowActivityIndicatorView(true)
//                imgTitle.sd_setIndicatorStyle(.gray)
//                imgTitle.sd_setImage(with: imgUrl, completed: nil)
//            }
//
//            if let welcomeText = objData?.heading {
//                self.lblWelcome.text = welcomeText
//            }
            if let emailPlaceHolder = objData?.emailPlaceholder {
                self.txtEmail.placeholder = emailPlaceHolder
            }
            if let passwordPlaceHolder = objData?.passwordPlaceholder {
                self.txtPassword.placeholder = passwordPlaceHolder
            }
            if let mobileNumber = objData?.mobile_number {
                self.oltMobNo.text = mobileNumber
            }
            
            if let password = objData?.password {
              self.oltPassword.text = password
            }
            if let submitText = objData?.formBtn {
            self.oltSubmitBtn.setTitle(submitText, for: .normal)
            }
           
            if let registerText = objData?.registerText {
              self.oltRegWithUs.setTitle(registerText, for: .normal)
            }
            
            if let connectOtherWays = objData?.connect_with_other_ways {
                self.oltConnectOtherWays.text = connectOtherWays
            }
            
            if let forgotPswrd = objData?.forgotText {
                self.oltForgotPswrd.setTitle(forgotPswrd, for: .normal )
            }
            
            
            
            // Show hide guest button
            guard let settings = defaults.object(forKey: "settings") else {
                return
            }
            let  settingObject = NSKeyedUnarchiver.unarchiveObject(with: settings as! Data) as! [String : Any]
            let objSettings = SettingsRoot(fromDictionary: settingObject)

            
            var isShowGuestButton = false
            if let isShowGuest = objSettings.data.isAppOpen {
                isShowGuestButton = isShowGuest
            }
            if isShowGuestButton {
              //  self.buttonGuestLogin.isHidden = true
                if let guestText = objData?.guestLogin {
                    //self.buttonGuestLogin.setTitle(guestText, for: .normal)
                }
            }
            else {
                 // self.buttonGuestLogin.isHidden = true
            }

            // Show/hide google and facebook button
            var isShowGoogle = false
            var isShowFacebook = false
            
            if let isGoogle = objSettings.data.registerBtnShow.google {
                isShowGoogle = isGoogle
            }
            if let isFacebook = objSettings.data.registerBtnShow.facebook{
                isShowFacebook = isFacebook
            }
            if isShowFacebook || isShowGoogle {
                if let sepratorText = objData?.separator {
                   
                }
            }
      
        }
    }
    
    //MARK:- IBActions
    
    @IBAction func actionForgotPassword(_ sender: Any) {
        let forgotPassVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(forgotPassVC, animated: true)
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        view.endEditing(true)
        self.adForest_logIn()
    }
    
    func adForest_logIn() {
        guard let email = txtEmail.text else {
            return
        }
        guard let password = txtPassword.text else {
            return
        }
        guard let countryCode = btnCountry.titleLabel?.text! else {
            return
        }
        if email == "" {
            self.txtEmail.shake(6, withDelta: 10, speed: 0.06)
        }
        else if password == "" {
             self.txtPassword.shake(6, withDelta: 10, speed: 0.06)
        }
        else if countryCode == "" {
            self.countryCode.shake(6, withDelta: 10, speed: 0.06)
        }
        else {
            
            let param : [String : Any] = [
                                            "email" : countryCode +  email,
                                            "password": password
                                        ]
            
          
            self.defaults.set(password, forKey: "password")
            self.adForest_loginUser(parameters: param as NSDictionary)
        }
    }
    
    @IBAction func actionFBLogin(_ sender: Any) {
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            if error != nil {
                print(error?.localizedDescription ?? "Nothing")
            }
            else if (result?.isCancelled)! {
                print("Cancel")
            }
            else if error == nil {
                self.userProfileDetails()
            } else {
            }
        }
    }
    
    @IBAction func actionGoogleLogin(_ sender: Any) {
        if GoogleAuthenctication.isLooggedIn {
            GoogleAuthenctication.signOut()
        }
        else {
            GoogleAuthenctication.signIn()
        }
    }
    
    @IBAction func actnLine(_ sender: UIButton) {
        
        LoginManager.shared.login(permissions: [.profile], in: self) {
            result in
            switch result {
            case .success(let loginResult):
                print(loginResult.accessToken.value)
                var email = ""
                if   let user_id = loginResult.userProfile?.userID {
                    email = user_id + "@line.com"
                }
                var userName = ""
                if  let username = loginResult.userProfile?.displayName {
                    userName = username
                }
                var id = ""
                if  let ido = loginResult.userProfile?.userID {
                    id = ido
                }
                let param: [String: Any] = [
                    "email": email,
                    "user_name" : userName,
                    "social_id" : id,
                    "type": "social"
                    
                ]
                print(param)
                self.defaults.set(true, forKey: "isSocial")
                self.defaults.set(id, forKey: "social_id")
                self.defaults.set(email, forKey: "email")
                self.defaults.set("1122", forKey: "password")
                self.defaults.synchronize()
                self.adForest_loginUser(parameters: param as NSDictionary)
                break;
                
            // Do other things you need with the login result
            case .failure(let error):
                print(error)
            }
        }
    }

    
    @IBAction func actionGuestLogin(_ sender: Any) {
       // buttonGuestLogin.isHidden = true
//        defaults.set(true, forKey: "isGuest")
//        self.showLoader()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            self.appDelegate.moveToHome()
//            self.stopAnimating()
//        }
    }
    
    @IBAction func actionRegisterWithUs(_ sender: Any) {
        let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "RegViewController") as! RegViewController
        self.navigationController?.pushViewController(registerVC, animated: true)
    }

    //MARK:- Google Delegate Methods
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
        }
        if error == nil {
            guard let googleID = user.userID,
                let name = user.profile.name
                else { return }
            guard let token = user.authentication.idToken else {
                return
            }
            var email = user.profile.email ?? ""
            if email == "" {
                email = googleID + "@gmail.com"
            }
        
            let param: [String: Any] = [
                                        "email": email ,
                                        "user_name": name,
                                        "social_id" : googleID,
                                        "type": "social"
                                        ]
         
            self.defaults.set(true, forKey: "isSocial")
             self.defaults.set(googleID, forKey: "social_id")
            self.defaults.set(email, forKey: "email")
            self.defaults.set("1122", forKey: "password")
            self.defaults.synchronize()
            self.adForest_loginUser(parameters: param as NSDictionary)
    }
}
    // Google Sign In Delegate
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Facebook Delegate Methods
    
    func userProfileDetails() {
        
        if (FBSDKAccessToken.current() != nil) {
            self.showLoader()
            FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, first_name, last_name, email, gender, picture.type(large)"]).start { (connection, result, error) in
                self.stopAnimating()
                if error != nil {
                    print(error?.localizedDescription ?? "Nothing")
                    return
                }
                else {
                    guard let results = result as? NSDictionary else { return }
                    guard let facebookId = results["id"] as? String else {
                            return
                    }
                    var email = results["email"] as? String ?? ""
                    let name = results["name"] as? String ?? ""
                    if email == "" {
                        email = facebookId + "@facebook.com"
                    }
                    
                    print("\(email), \(facebookId)")
                    let param: [String: Any] = [
                        "email": email,
                        "user_name" : name,
                        "social_id" : facebookId,
                        "type": "social"
                    ]
                    print(param)
                    self.defaults.set(true, forKey: "isSocial")
                    self.defaults.set(email, forKey: "email")
                    self.defaults.set(facebookId, forKey: "social_id")
                    self.defaults.set("1122", forKey: "password")
                    self.defaults.synchronize()
                    
                    self.adForest_loginUser(parameters: param as NSDictionary)
                }
            }
        }
    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
    
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        
        return true
    }
    
    
    //MARK:- API Calls
    
    //Login Data Get Request
    func adForest_loginDetails() {
        self.showLoader()
        UserHandler.loginDetails(success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                UserHandler.sharedInstance.objLoginDetails = successResponse.data
                self.adForest_populateData()
            } else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            self.stopAnimating()
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    // Login User
    func adForest_loginUser(parameters: NSDictionary) {
        self.showLoader()
        UserHandler.loginUser(parameter: parameters , success: { (successResponse) in
            self.stopAnimating()
            print(successResponse)
            if successResponse.success {
                if self.isVerifyOn && successResponse.data.isAccountConfirm == false {
                    userId = successResponse.data.id
                    let alert = AlertView.prepare(title: "", message: successResponse.message, okAction: {
                        let confirmationVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
                        confirmationVC.isFromVerification = true
                        confirmationVC.user_id = successResponse.data.id
                          userId = successResponse.data.id
                        
                        self.navigationController?.pushViewController(confirmationVC, animated: true)
                    })
                    self.presentVC(alert)
                } else {
                    self.defaults.set(true, forKey: "isLogin")
                    self.defaults.set(successResponse.data.id, forKey: "user_id")
                    self.defaults.set(successResponse.data.userEmail, forKey: "email")
                     userId = successResponse.data.id
                    
                    self.defaults.synchronize()
                    self.appDelegate.moveToHome()
                }
            } else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
}

extension LoginViewController {
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        //Mark:- Max character length in a textfield
        if textField == countryCode{
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
           let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }

      //Mark:- textfield will not contain '+' sign
        if textField == txtEmail{
            if (txtEmail.text?.contains("+"))!{
                guard let valuetxt = txtEmail.text else { return true}
                var txtfieldtxt: String = valuetxt
                txtfieldtxt.remove(at: txtfieldtxt.index(before: txtfieldtxt.endIndex))
                textField.text = txtfieldtxt
            }
        }
        return true
    }
}
