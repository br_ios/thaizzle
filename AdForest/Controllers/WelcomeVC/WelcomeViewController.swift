//
//  WelcomeViewController.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 15/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
 var defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
     navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    

    @IBAction func thaiLanguageBtn(_ sender: UIButton) {
    
        self.defaults.setValue("th",forKey: Constants.APP.lang)
         AddsHandler.sharedInstance.objAdPost = nil
           self.settingsdata()
    }
    

    
    @IBAction func englishLanguageBtn(_ sender: UIButton) {
        self.defaults.setValue("en",forKey: Constants.APP.lang)
        AddsHandler.sharedInstance.objAdPost = nil
            self.settingsdata()
 
    }
    //MARK:- API Call
    func settingsdata() {
      
        UserHandler.settingsdata(success: { (successResponse) in
       
            if successResponse.success {
                //Change App Color Here
                self.defaults.set(successResponse.data.mainColor, forKey: "mainColor")
                self.appDelegate.customizeNavigationBar(barTintColor: Constants.hexStringToUIColor(hex: successResponse.data.mainColor))
                self.defaults.set(successResponse.data.isRtl, forKey: "isRtl")
                self.defaults.set(successResponse.data.notLoginMsg, forKey: "notLogin")
                self.defaults.set(successResponse.data.isAppOpen, forKey: "isAppOpen")
                self.defaults.set(successResponse.data.showNearby, forKey: "showNearBy")
                self.defaults.set(successResponse.data.appPageTestUrl, forKey: "shopUrl")
                //Save Shop title to show in Shop Navigation Title
                self.defaults.set(successResponse.data.menu.shop, forKey: "shopTitle")
              
                //Offers title
                self.defaults.set(successResponse.data.messagesScreen.mainTitle, forKey: "message")
                self.defaults.set(successResponse.data.messagesScreen.sent, forKey: "sentOffers")
                self.defaults.set(successResponse.data.messagesScreen.receive, forKey: "receiveOffers")
                self.defaults.synchronize()
                UserHandler.sharedInstance.objSettings = successResponse.data
                UserHandler.sharedInstance.objSettingsMenu = successResponse.data.menu.submenu.pages
                UserHandler.sharedInstance.menuKeysArray = successResponse.data.menu.dynamicMenu.keys
                UserHandler.sharedInstance.menuValuesArray = successResponse.data.menu.dynamicMenu.array
                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NotificationName.updateUserProfile), object: nil)
                   self.appDelegate.moveToHome()
            } else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
           
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
}
