//
//  VerifyCodeVC.swift
//  AdForest
//
//  Created by Admin 5 on 09/07/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class VerifyCodeVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var txtfldOtp: UITextField!
    
    var Phonenumber = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         txtfldOtp.delegate = self
         print(Phonenumber)
        
    }
    
    func VerifyApi(Code:String)
    {
        
        UserHandler.VerifyOtp(Code: Code, Phoneno: Phonenumber, success: { (successResponse) in
         if successResponse.success == true {
            print(successResponse.userId)
            
            var userId =  successResponse.userId
            print(userId)
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmNewPasswordVC") as! ConfirmNewPasswordVC
            vc.ConfirmUserId = userId!
            self.navigationController?.pushViewController(vc, animated: true)
            
            }
            else {
                
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            
             //  let alert = Constants.showBasicAlert(message: error.message)
             //  self.presentVC(alert)
            }
    }

    @IBAction func actionVerify(_ sender: Any) {
        if txtfldOtp.text == "" {
            let alert = Constants.showBasicAlert(message: "Please Enter Otp")
            
        }else{
            VerifyApi(Code: txtfldOtp.text!)
        }
    }
    
    
    
}
