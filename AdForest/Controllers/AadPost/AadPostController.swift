//
//  AadPostController.swift
//  AdForest
//
//  Created by apple on 4/25/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Photos
import NVActivityIndicatorView
import Alamofire
import OpalImagePicker
import UITextField_Shake
import MapKit
import TextFieldEffects
import DropDown
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
import SDWebImage

class AadPostController: UIViewController, NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource, textFieldValueDelegate, PopupValueChangeDelegate, OpalImagePickerControllerDelegate, UINavigationControllerDelegate, textViewValueDelegate
{
    
    //MARK:- Outlets   //MAYURRR
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            // tableView.tableFooterView = UIView()
          //  tableView.separatorStyle = .singleLine
           // tableView.separatorColor = UIColor.black
            tableView.showsVerticalScrollIndicator = false
            tableView.register(UINib(nibName: "AdPostURLCell", bundle: nil), forCellReuseIdentifier: "AdPostURLCell")
            tableView.register(UINib(nibName: "CalendarCell", bundle: nil), forCellReuseIdentifier: "CalendarCell")
        }
    }
    @IBOutlet weak var tblCategory: UITableView!
    
    //MARK:- Properties
    var isFromEditAd = false
    var ad_id = 0
    var catID = ""
    var dataArray = [AdPostField]()
    var newArray = [AdPostField]()
    var imagesArray = [AdPostImageArray]()
    var dynamicArray = [AdPostField]()
    var hasPageNumber = ""
    var refreshArray = [AdPostField]()
    var imageIDArray = [Int]()
    var data = [AdPostField]()
    let defaults = UserDefaults.standard
    var selectTxt = String()
    @objc var selectedcategory = String()
    
    // Empty Fields Check
    var adTitle = ""
    
    //MARK:- View Life Cycle
    //MARK:- Properties
    var photoArray = [UIImage]()
    
    var imageArray = [AdPostImageArray]()
    
    var fieldsArray = [AdPostField]()
    var adID = 0
    //this array get data from previous controller
    var objArray = [AdPostField]()
    var customArray = [AdPostField]()
    var haspageNumber = ""
    var localArray = AddsHandler.sharedInstance.objAdPostData
    var valueArray = [String]()
    var maximumImagesAllowed = 0
    var localVariable = ""
    var localDictionary = [String: Any]()
    var isfromEditAd = false
    
    var isFromAddData = ""
    var popUpTitle = ""
    var selectedIndex = 0
    
    var isValidUrl = false
    
    
    
    // third vc
    
    //MARK:- Properties
    let locationDropDown = DropDown()
    lazy var dropDowns : [DropDown] = {
        return [
            self.locationDropDown
        ]
    }()
    
    var popUpArray = [String]()
    var hasSubArray = [Bool]()
    var locationIdArray = [String]()
    
    var hasSub = false
    var selectedID = ""
    var popUpConfirm = ""
    var popUpCancel = ""
    var popUpText = ""
    
    let locationManager = CLLocationManager()
    let newPin = MKPointAnnotation()
    let regionRadius: CLLocationDistance = 1000
    var initialLocation = CLLocation(latitude: 25.276987, longitude: 55.296249)
    
    var latitude = ""
    var longitude = ""
    var descriptionText = ""
    
    var addInfoDictionary = [String: Any]()
    var customDictionary = [String: Any]()
    var SelectedCategoryIndex = Int()
    //  var customArray = [AdPostField]()
    //   var imageArray = AddsHandler.sharedInstance.adPostImagesArray
    // get values in populate data and send it with parameters
    var phone_number = ""
    var address = ""
    
    var isFeature = "false"
    var isBump = false
    
    var selectedCountry = ""
    
    var data1 = [AdPostField]()
    var dataArray1 = [AdPostField]()
    var dropDownValuesArray = [String]()
    var dropDownImagesArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showBackButton()
        //self.forwardButton()
        tblCategory.isHidden = true
        self.googleAnalytics(controllerName: "Add Post Controller")
        if isFromEditAd {
            let param: [String: Any] = ["is_update": ad_id]
            print(param)
            self.adForest_adPost(param: param as NSDictionary)
        }
        else {
            let param: [String: Any] = ["is_update": ""]
            print(param)
           
                self.adForest_adPost(param: param as NSDictionary)

            
        }
        
        self.hideKeyboard()
       // self.adForest_populateData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(onForwardButtonClciked), name: NSNotification.Name(rawValue: "Monthyear"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         AddsHandler.sharedInstance.objAdPost = nil
        imageArray.removeAll()
        
    }
    
    
    func adForest_populateData() {
        if  AddsHandler.sharedInstance.objAdPost != nil {
            let objData = AddsHandler.sharedInstance.objAdPost
            if let titleText = objData?.data.title {
                self.title = titleText
            }
            if let id = objData?.data.adId {
                self.adID = id
            }
            if let maximumImages = objData?.data.images.numbers {
                self.maximumImagesAllowed = maximumImages
            }
        }
    }
    
    //MARK: - Custom
    func showLoader() {
        self.startAnimating(Constants.activitySize.size, message: Constants.loaderMessages.loadingMessage.rawValue,messageFont: UIFont.systemFont(ofSize: 14), type: NVActivityIndicatorType.ballClipRotatePulse)
    }
    
    func forwardButton() {
        let button = UIButton(type: .custom)
        if #available(iOS 11, *) {
            button.widthAnchor.constraint(equalToConstant: 30).isActive = true
            button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        }
        else {
            button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        }
        if defaults.bool(forKey: "isRtl") {
            button.setBackgroundImage(#imageLiteral(resourceName: "backbutton"), for: .normal)
        } else {
            button.setBackgroundImage(#imageLiteral(resourceName: "forwardButton"), for: .normal)
        }
        button.addTarget(self, action: #selector(onForwardButtonClciked), for: .touchUpInside)
        let forwardBarButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = forwardBarButton
    }
    
    func changeText(value: String, fieldTitle: String) {
        for index in 0..<dataArray.count {
            if let objData = dataArray[index] as? AdPostField {
                if objData.fieldType == "textfield" {
                    if let cell  = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? AdPostCell {
                        var obj = AdPostField()
                        obj.fieldVal = value
                        obj.fieldTypeName = cell.fieldName
                        obj.fieldType = "textfield"
                        objArray.append(obj)
                        data.append(obj)
                        if fieldTitle == self.dataArray[index].fieldTypeName {
                            self.dataArray[index].fieldVal = value
                          
                        }
                    }
                    else
                    {
                        var obj = AdPostField()
                        obj.fieldVal = value
                        obj.fieldTypeName = fieldTitle
                        obj.fieldType = "textfield"
                            objArray.append(obj)
                            customArray.append(obj)
                        if fieldTitle == self.dataArray[index].fieldTypeName {
                            self.dataArray[index].fieldVal = value
                        }
                    }
                  //  else if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 3)) as? TextFieldCell {
//                        var obj = AdPostField()
//                        obj.fieldType = "textfield"
//                        obj.fieldVal = value
//                        obj.fieldTypeName = cell.fieldName
//                        objArray.append(obj)
//                        customArray.append(obj)
//
//                        if fieldTitle == self.dataArray[index].fieldTypeName {
//                            self.dataArray[index].fieldVal = value
//                        }
//                    }
                  
                    
                   
                }
            }
        }
    }
    
    func changePopupValue(selectedKey: String, fieldTitle: String, selectedText: String)
    {
        print(selectedKey, fieldTitle, selectedText)
        self.selectTxt = selectedText
        
        for index in 0..<dataArray.count
        {
            if let objData = dataArray[index] as? AdPostField {
                if objData.fieldType == "select" {
                    if let cell  = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? AdPostPopupCell {
                        var obj = AdPostField()
                        obj.fieldVal = selectedKey
                        obj.fieldTypeName = fieldTitle
                        obj.fieldType = "select"
                        data.append(obj)
                        
                        if fieldTitle == self.dataArray[index].fieldTypeName {
                            self.dataArray[index].fieldVal = selectedText
                            cell.oltPopup.setTitle(selectedText, for: .normal)
                        }
                    }
                }
            }
        }
        self.onForwardButtonClciked()
    }
    
    
    @objc func onForwardButtonClciked()
    {
        var data = [AdPostField]()
        for index in  0..<dataArray.count {
            if let objData = dataArray[index] as? AdPostField
            {
                if objData.fieldType == "textfield"
                {
                    if let cell  = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? AdPostCell
                    {
                        var obj = AdPostField()
                        obj.fieldVal = cell.txtType.text
                        obj.fieldTypeName = cell.fieldName
                        obj.fieldType = "textfield"
                        data.append(obj)
                        
                        guard let txtTitle = cell.txtType.text else {return}
                        if cell.fieldName == "ad_title" {
                            if txtTitle == "" {
                                cell.txtType.shake(6, withDelta: 10, speed: 0.06)
                            } else {
                                self.adTitle = txtTitle
                            }
                        }
                        
                    }
                }
                else if objData.fieldType == "select" {
                    if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? AdPostPopupCell {
                        var obj = AdPostField()
                        obj.fieldVal = cell.selectedKey
                        obj.fieldTypeName = cell.fieldName
                        obj.fieldType = "select"
                        print(obj.fieldVal, obj.fieldTypeName, obj.fieldType)
                        data.append(obj)
                    }
                }
            }
        }
        if self.adTitle == "" {
            
        }
        else {
            
            if AddsHandler.sharedInstance.isCategoeyTempelateOn {
                print(data)
                print(self.dataArray)
                print(self.isFromEditAd)
                self.refreshArray = dataArray
                self.refreshArray.insert(contentsOf: AddsHandler.sharedInstance.objAdPostData, at: 2)
                self.fieldsArray = self.refreshArray
                self.objArray = data
                self.isfromEditAd = self.isFromEditAd
            }
            else {
                print(data)
                print(self.dataArray)
                print(self.isFromEditAd)
                self.objArray = data
                self.fieldsArray = self.dataArray
                 self.isfromEditAd = self.isFromEditAd
            }
            //            self.imageArray = self.imagesArray
            //            self.imageIDArray = self.imageIDArray
            //            self.navigationController?.pushViewController(postVC, animated: true)
            
            self.dataArray1 = fieldsArray
            
            let valuesArray = ["ad_title","ad_cats1" ,"ad_price_type", "ad_price", "ad_currency", "ad_condition", "ad_warranty", "ad_type", "ad_yvideo", "checkbox"]
            let valueToremove = ["ad_title","ad_cats1", "ad_currency"]
            
            for item in dataArray1 {
                if AddsHandler.sharedInstance.isCategoeyTempelateOn ==  false {
                    if valuesArray.contains(item.fieldTypeName) {
                        let value = item.fieldTypeName
                        let index = dataArray1.index{ $0.fieldTypeName ==  value}
                        if let index = index {
                            dataArray1.remove(at: index)
                        }
                    }
                }
                else {
                    if valueToremove.contains(item.fieldTypeName) {
                        let value = item.fieldTypeName
                        let index = dataArray1.index{ $0.fieldTypeName ==  value}
                        if let index = index {
                            dataArray1.remove(at: index)
                        }
                    }
                }
            }
            fieldsArray = dataArray1
            
            //   self.adForest_populateData()
            //            let map = GMSMapView()
            //            map.isMyLocationEnabled = true
            //            map.settings.myLocationButton = true
            
            
            self.tableView.reloadData()
            self.selectedcategory = ""
        }
    }
    
    //MARK:- Table View Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblCategory
        {
            return 1
        }
        else
        {
            if fieldsArray.count == 0
            {
                return 3
            }
            return 5
        }
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblCategory {
            return dropDownValuesArray.count
        }
        else
        {
            var returnValue = 0
            if fieldsArray.count == 0
            {
                if section == 0
                {
                    if hasPageNumber == "1" {
                        return dataArray.count
                    }
                    return dataArray.count
                }
               else if section == 1 {
                    returnValue =  1
                }else if section == 2 {
                    returnValue =  1
                }
                return returnValue
            }
            else
            {
                if section == 0
                {
                    if hasPageNumber == "1" {
                        return dataArray.count
                    }
                    return dataArray.count
                }
                if section == 1 {
                    returnValue =  1
                }
                else if section == 2 {
                    returnValue =  1
                }
                else if section == 3
                {
                    returnValue = fieldsArray.count
                }
                else if section == 4
                {
                    returnValue = 1
                }
                return returnValue
            }
        }
       
        
    }
    
    @IBAction func selectCategory(sender:UIButton)
    {
        print("tag\(sender.tag)")
        let hitPoint = sender.convert(CGPoint.zero, to: self.tblCategory)
        if let indexPath = tblCategory.indexPathForRow(at: hitPoint)
        {
            print("indexPath\(indexPath)")
                        selectedcategory = dropDownValuesArray[indexPath.row]
                        SelectedCategoryIndex = indexPath.row
          
                        print(SelectedCategoryIndex)
                        tblCategory.isHidden = true
                        self.tableView.isScrollEnabled = true
                        self.tableView.reloadData()

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblCategory
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCategoryDetailCell", for: indexPath) as! SearchCategoryDetailCell
            cell.lblName?.text = dropDownValuesArray[indexPath.row]
          //  cell.Imgcategory?.sd_setImage(with: URL(string: dropDownImagesArray[indexPath.row]))
          
            cell.Imgcategory.sd_setImage(with: URL(string: dropDownImagesArray[indexPath.row]), placeholderImage:UIImage(named: "jobImg"))

    
        return cell
            
        }else{
        
        let section = indexPath.section
        if fieldsArray.count == 0
        {
            if section == 0
            {
                let objData = dataArray[indexPath.row]
                if objData.fieldType == "textfield"  && objData.hasPageNumber == "1" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AdPostCell", for: indexPath) as! AdPostCell
                    
                    if let title = objData.title  {
                        cell.txtType.placeholder = title
                    }
                    if let fieldValue = objData.fieldVal {
                        cell.txtType.text = fieldValue
                    }
                    cell.fieldName = objData.fieldTypeName
                    print(cell.fieldName)
                    cell.delegateText = self
                    return cell
                }
                else if objData.fieldType == "select" && objData.hasPageNumber == "1"  {
                    let cell: AdPostPopupCell = tableView.dequeueReusableCell(withIdentifier: "AdPostPopupCell", for: indexPath) as! AdPostPopupCell
                    
                    if let title = objData.title {
                        cell.lblType.text = title
                    }
                    
                    if let fieldValue = objData.fieldVal {
                        cell.oltPopup.setTitle(fieldValue, for: .normal)
                    }
                    var i = 1
                    for item in objData.values {
                        if item.id == "" {
                            continue
                        }
                        if i == 1 {
                       //     cell.oltPopup.setTitle(item.name, for: .normal)
                            cell.selectedKey = String(item.id)
                        }
                        i = i + 1
                    }
                    
                    cell.btnPopUpAction = { () in
                      
                        for index in  0..<self.dataArray.count {
                            let cell1  = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? AdPostCell
                            let txtTitle = cell1?.txtType.text
                            if cell1?.fieldName == "ad_title" {
                                if txtTitle == "" {
                                    cell1?.txtType.shake(6, withDelta: 10, speed: 0.06)
                                    let alert = AlertView.prepare(title: "", message:"Please Enter Ad_Title", okAction: {
                                    })
                                    self.presentVC(alert)
                                }
                            }else {
                                cell.dropDownKeysArray = []
                                cell.dropDownValuesArray = []
                                cell.hasCatTemplateArray = []
                                cell.hasTempelateArray = []
                                cell.hasSubArray = []
                                for items in objData.values {
                                    if items.id == "" {
                                        continue
                                    }
                                    cell.dropDownKeysArray.append(String(items.id))
                                    cell.dropDownValuesArray.append(items.name)
                                    cell.dropDownImagesArray.append(items.ImageDrop)
                                    
                                    cell.hasCatTemplateArray.append((objData.hasCatTemplate != nil))
                                    cell.hasTempelateArray.append(items.hasTemplate)
                                    cell.hasSubArray.append(items.hasSub)
                                    
                                    
                                }
                              self.dropDownValuesArray = cell.dropDownValuesArray
                                 self.dropDownImagesArray = cell.dropDownImagesArray
                                self.tblCategory.isHidden = false
                                //self.tableView.isScrollEnabled = false
                                self.adForest_populateData()
                                self.tblCategory.reloadData()
//                                cell.popupShow()
//                                cell.selectionDropdown.show()
                            }
                            
                        }
                        
                        //                        cell.dropDownKeysArray = []
                        //                        cell.dropDownValuesArray = []
                        //                        cell.hasCatTemplateArray = []
                        //                        cell.hasTempelateArray = []
                        //                        cell.hasSubArray = []
                        //                        for items in objData.values {
                        //                            if items.id == "" {
                        //                                continue
                        //                            }
                        //                            cell.dropDownKeysArray.append(String(items.id))
                        //                            cell.dropDownValuesArray.append(items.name)
                        //                            cell.hasCatTemplateArray.append(objData.hasCatTemplate)
                        //                            cell.hasTempelateArray.append(items.hasTemplate)
                        //                            cell.hasSubArray.append(items.hasSub)
                        //                        }
                        //                        cell.popupShow()
                        //                        cell.selectionDropdown.show()
                        
                    }
                  
                    cell.fieldName = objData.fieldTypeName
                    cell.delegatePopup = self
                    if self.selectedcategory != "" {
                        print(selectedcategory)
                        cell.Selectedcategory(item: selectedcategory, index: SelectedCategoryIndex)
                    }
                    return cell
                }
            }
            else if section == 1 {
                
                let cell: UploadImageCell = tableView.dequeueReusableCell(withIdentifier: "UploadImageCell", for: indexPath) as! UploadImageCell
                
                let objData = AddsHandler.sharedInstance.objAdPost
                
                if let imagesTitle = objData?.extra.imageText {
                    cell.lblSelectImages.text = imagesTitle
                }
                
                cell.lblPicNumber.text = String(photoArray.count)
                
                cell.btnUploadImage = { () in
                    print(self.objArray)
                   // if objData.title
                    let imagePicker = OpalImagePickerController()
                    
                    imagePicker.navigationBar.tintColor = UIColor.white
                    
                    imagePicker.maximumSelectionsAllowed = 8
                    print(self.maximumImagesAllowed)
                    imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
                    // maximum message
                    let configuration = OpalImagePickerConfiguration()
                    configuration.maximumSelectionsAllowedMessage = NSLocalizedString((objData?.data.images.message)!, comment: "")
                    imagePicker.configuration = configuration
                    imagePicker.imagePickerDelegate = self
                    self.present(imagePicker, animated: true, completion: nil)
                    
                }
                return cell
                
            }
            else if section == 2
            {
//                let cell: ThirdViewTablecell = tableView.dequeueReusableCell(withIdentifier: "mapCell", for: indexPath) as! ThirdViewTablecell
//                return cell
                    let cell: ThirdViewTablecell = tableView.dequeueReusableCell(withIdentifier: "mapCell", for: indexPath) as! ThirdViewTablecell
                    
                
                    cell.oltCheck.addTarget(self, action: #selector(oltCheck), for: .touchUpInside)
                    cell.oltPostAdd.addTarget(self, action: #selector(oltPostAdd), for: .touchUpInside)
                    //  cell.ChkBtn_Out.addTarget(self, action: #selector(CheckOutBtn), for:.touchUpInside)
                    
                    if AddsHandler.sharedInstance.objAdPost != nil {
                        let objData = AddsHandler.sharedInstance.objAdPost
                        
                        if let pageTitle = objData?.data.title {
                            self.title = pageTitle
                        }
                        if let infoText = objData?.extra.userInfo {
                            cell.lblUserinfo.text = infoText
                        }
                        if let nameText = objData?.data.profile.name.title {
                            cell.txtName.placeholder = nameText
                        }
                        if  let name = objData?.data.profile.name.fieldVal {
                            cell.txtName.text = name
                        }
                        
                        if let nmbrText = objData?.data.profile.phone.title {
                            cell.txtNumber.placeholder = nmbrText
                        }
                        if let nmbr = objData?.data.profile.phone.fieldVal {
                            cell.txtNumber.text = nmbr
                            self.phone_number = nmbr
                        }
                        
                        var isPhoneEdit = false
                        if let editNumber = objData?.data.profile.phoneEditable {
                            isPhoneEdit = editNumber
                        }
                        
                        if isPhoneEdit {
                            cell.txtNumber.isEnabled = true
                        }
                        else if isPhoneEdit == false {
                            cell.txtNumber.isEnabled = false
                        }
                        
                        if let addressText = objData?.data.profile.location.title {
                            cell.txtAddress.placeholder = addressText
                        }
                        
                        if let addressValue = objData?.data.profile.location.fieldVal {
                            cell.txtAddress.text = addressValue
                            self.address = addressValue
                            if cell.txtAddress.text == "" {
                                print(selectedAddress)
                                cell.txtAddress.text = selectedAddress
                                self.address = selectedAddress
                            }
                        }
                        
                        //guard
                        let isMapShow = objData?.data.profile.map.onOff
                        //                        else {
                        //                        return
                        //                    }
                        
                        if isMapShow! {
                            
                            if let latText = objData?.data.profile.map.locationLat.title {
                                cell.txtLatitude.placeholder = latText
                            }
                            
                            if let latValue =  objData?.data.profile.map.locationLat.fieldVal {
                                cell.txtLatitude.text = latValue
                            }
                            
                            if let longText = objData?.data.profile.map.locationLong.title {
                                cell.txtLongitude.placeholder = longText
                            }
                            
                            if let longValue = objData?.data.profile.map.locationLong.fieldVal {
                                cell.txtLongitude.text = longValue
                            }
                            if let lat = objData?.data.profile.map.locationLat.fieldVal {
                                self.latitude = lat
                            }
                            if let long = objData?.data.profile.map.locationLong.fieldVal {
                                self.longitude = long
                            }
                            
                            if latitude != "" && longitude != "" {
                                initialLocation = CLLocation(latitude: Double(latitude)!, longitude: Double(longitude)!)
                            }
                            cell.centerMapOnLocation(location: initialLocation)
                            cell.addAnnotations(coords: [initialLocation])
                            cell.setupView()
                        }
                            
                        else if isMapShow == false {
                            cell.containerViewMap.isHidden = true
                            cell.containerViewFeatureAdd.translatesAutoresizingMaskIntoConstraints = false
                            cell.containerViewFeatureAdd.topAnchor.constraint(equalTo: cell.containerViewAddress.bottomAnchor, constant: 8).isActive = true
                        }
                        
                        //                    guard
                        let isShowCountry = objData?.data.profile.adCountryShow
                        //else {
                        //                        return
                        //                    }
                        if isShowCountry! {
                            for values in (objData?.data.profile.adCountry.values)! {
                                var i = 1
                                if values.id == "" {
                                    continue
                                }
                                cell.popUpArray.append(values.name)
                                cell.hasSubArray.append(values.hasSub)
                                cell.locationIdArray.append(String(values.id))
                                if i == 1 {
                                    //  cell.oltPopup.setTitle(values.name, for: .normal)
                                }
                                //  cell.popUpArray = popUpArray
                                i = i + 1
                            }
                            //location popup method call here after asigning data
                            cell.locationPopup()
                        }
                           
                       
                        
                        else if isShowCountry == false {
                            cell.containerViewPopup.isHidden = true
                            cell.containerViewAddress.translatesAutoresizingMaskIntoConstraints = false
                            cell.containerViewAddress.topAnchor.constraint(equalTo: cell.txtNumber.bottomAnchor, constant: 8).isActive = true
                        }
                        
                        cell.btnLocationPopUpAction = { () in
                           // cell.locationPopup()
                            cell.locationDropDown.show()
                            
                        }
                        
                        if let locationText = objData?.data.profile.adCountry.title {
                            cell.lblLocation.text = locationText
                        }
                        
                        //                    guard
                        let featuredAdBuy = objData?.data.profile.featuredAdBuy
                        //  else {
                        //                        return
                        //                    }
                        
                        if featuredAdBuy! {
                            if let checkButtonTitle = objData?.data.profile.featuredAdNotify.btn {
                                cell.oltCheck.setTitle(checkButtonTitle, for: .normal)
                                cell.oltCheck.backgroundColor = Constants.hexStringToUIColor(hex: "#00a2ff")
                                // imgCheckBox.image = nil
                            }
                            if let featureTitle = objData?.data.profile.featuredAdNotify.text {
                                cell.lblFeatureAdd.text = featureTitle
                            }
                        }
                        
                        //                    guard
                        let featureAdShow = objData?.data.profile.featuredAdIsShow
                        //else {
                        //                        return
                        //                    }
                        
                        if featureAdShow! {
                            cell.imgCheckBox.image = #imageLiteral(resourceName: "uncheck")
                            if let featureAdText = objData?.data.profile.featuredAd.title {
                                cell.lblFeatureAdd.text = featureAdText
                            }
                            
                            if let titlePop = objData?.data.profile.featuredAdText.title {
                                self.popUpTitle = titlePop
                            }
                            if let textPop = objData?.data.profile.featuredAdText.text {
                                self.popUpText = textPop
                            }
                            if let okPop = objData?.data.profile.featuredAdText.btnOk {
                                self.popUpConfirm = okPop
                            }
                            
                            if let cancelPop = objData?.data.profile.featuredAdText.btnNo {
                                self.popUpCancel = cancelPop
                            }
                        }
                        if featureAdShow == false && featuredAdBuy == false {
                            cell.containerViewFeatureAdd.isHidden = true
                        }
                        if let postButtonTitle =  objData?.data.btn_submit_post_ad {
                            cell.oltPostAdd.setTitle(postButtonTitle, for: .normal)
                        }
                        
                        //   guard
                        let isShowBump = objData?.data.profile.bumpAdIsShow
                        //                        else {
                        //                        return
                        //                    }
                        if isShowBump! {
                            cell.imgCheckBump.image = #imageLiteral(resourceName: "uncheck")
                            if let bumpText = objData?.data.profile.bumpAd.title {
                                cell.lblBumpText.text = bumpText
                            }
                        } else {
                            cell.containerViewBumpUpAds.isHidden = true
                            cell.containerViewFeatureAdd.translatesAutoresizingMaskIntoConstraints = false
                            cell.containerViewFeatureAdd.topAnchor.constraint(equalTo: cell.containerViewMap.bottomAnchor, constant: 8).isActive = true
                        }
                    }
                    
                    return cell
                
            }
            
            return UITableViewCell()
        }
            
        else
        {
            if section == 0
            {
                let objData = dataArray[indexPath.row]
                if objData.fieldType == "textfield"  && objData.hasPageNumber == "1" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AdPostCell", for: indexPath) as! AdPostCell
                    
                    if let title = objData.title  {
                        cell.txtType.placeholder = title
                    }
                    if let fieldValue = objData.fieldVal {
                        cell.txtType.text = fieldValue
                    }
                    cell.fieldName = objData.fieldTypeName
                    cell.delegateText = self
                    return cell
                }
                else if objData.fieldType == "select" && objData.hasPageNumber == "1"  {
                    let cell: AdPostPopupCell = tableView.dequeueReusableCell(withIdentifier: "AdPostPopupCell", for: indexPath) as! AdPostPopupCell
                    
                    if let title = objData.title {
                        cell.lblType.text = title
                    }
                    
                    if let fieldValue = objData.fieldVal {
                        cell.oltPopup.setTitle(fieldValue, for: .normal)
                    }
                    var i = 1
                    for item in objData.values {
                        if item.id == "" {
                            continue
                        }
                        if i == 1 {
                         //   cell.oltPopup.setTitle(item.name, for: .normal)
                            cell.selectedKey = String(item.id)
                        }
                        i = i + 1
                    }
                    
                    cell.btnPopUpAction = { () in
                        
                        for index in  0..<self.dataArray.count {
                            let cell1  = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? AdPostCell
                            let txtTitle = cell1?.txtType.text
                            if cell1?.fieldName == "ad_title" {
                                if txtTitle == "" {
                                    cell1?.txtType.shake(6, withDelta: 10, speed: 0.06)
                                    let alert = AlertView.prepare(title: "", message:"Please Enter Ad_Title", okAction: {
                                    })
                                    self.presentVC(alert)
                                }
                            }else {
                                cell.dropDownKeysArray = []
                                cell.dropDownValuesArray = []
                                cell.hasCatTemplateArray = []
                                cell.hasTempelateArray = []
                                cell.hasSubArray = []
                                for items in objData.values {
                                    if items.id == "" {
                                        continue
                                    }
                                    cell.dropDownKeysArray.append(String(items.id))
                                    cell.dropDownValuesArray.append(items.name)
                                    cell.dropDownImagesArray.append(items.ImageDrop)
                                    cell.hasCatTemplateArray.append((objData.hasCatTemplate != nil))
                                    cell.hasTempelateArray.append(items.hasTemplate)
                                    cell.hasSubArray.append(items.hasSub)
                                    
                                    
                                }
                                self.dropDownValuesArray = cell.dropDownValuesArray
                                self.dropDownImagesArray = cell.dropDownImagesArray
                                self.tblCategory.isHidden = false
                               // self.tableView.isScrollEnabled = false
                                self.adForest_populateData()
                                self.tblCategory.reloadData()
                                //                                cell.popupShow()
                                //                                cell.selectionDropdown.show()
                            }
                            
                        }
                        
                        //                        cell.dropDownKeysArray = []
                        //                        cell.dropDownValuesArray = []
                        //                        cell.hasCatTemplateArray = []
                        //                        cell.hasTempelateArray = []
                        //                        cell.hasSubArray = []
                        //                        for items in objData.values {
                        //                            if items.id == "" {
                        //                                continue
                        //                            }
                        //                            cell.dropDownKeysArray.append(String(items.id))
                        //                            cell.dropDownValuesArray.append(items.name)
                        //                            cell.hasCatTemplateArray.append(objData.hasCatTemplate)
                        //                            cell.hasTempelateArray.append(items.hasTemplate)
                        //                            cell.hasSubArray.append(items.hasSub)
                        //                        }
                        //                        cell.popupShow()
                        //                        cell.selectionDropdown.show()
                        
                    }
                    
                    cell.fieldName = objData.fieldTypeName
                    cell.delegatePopup = self
                   if self.selectedcategory != "" {
                       cell.Selectedcategory(item: selectedcategory, index: SelectedCategoryIndex)
                   }
                    return cell
                }
            }
            else if section == 1 {
                let cell: UploadImageCell = tableView.dequeueReusableCell(withIdentifier: "UploadImageCell", for: indexPath) as! UploadImageCell
                
                let objData = AddsHandler.sharedInstance.objAdPost
                
                if let imagesTitle = objData?.extra.imageText {
                    cell.lblSelectImages.text = imagesTitle
                }
                
                cell.lblPicNumber.text = String(imageArray.count)
                
                cell.btnUploadImage = { () in
                    print(objData?.data.fields)
                  
                    
                    
                    let imagePicker = OpalImagePickerController()
                    
                    imagePicker.navigationBar.tintColor = UIColor.white
                    
                    imagePicker.maximumSelectionsAllowed = 8
                    print(self.maximumImagesAllowed)
                    imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
                    // maximum message
                    let configuration = OpalImagePickerConfiguration()
                   // configuration.maximumSelectionsAllowedMessage = NSLocalizedString((objData?.data.images.message)!, comment: "")
                    imagePicker.configuration = configuration
                    imagePicker.imagePickerDelegate = self
                    self.present(imagePicker, animated: true, completion: nil)
                    
                }
                return cell
            }
                
            else if section == 2 {
                let cell: CollectionImageCell = tableView.dequeueReusableCell(withIdentifier: "CollectionImageCell", for: indexPath) as! CollectionImageCell
                let objData = AddsHandler.sharedInstance.objAdPost
                
                if let sortMsg = objData?.extra.sortImageMsg {
                    cell.lblArrangeImage.text = sortMsg
                }
                if let adID = objData?.data.adId {
                    cell.ad_id = adID
                }
                cell.dataArray = self.imageArray
                
                cell.collectionView.reloadData()
                return cell
            }
                
            else if section == 3 {
                let objData = fieldsArray[indexPath.row]
                print("objData.fieldType: \(objData.fieldType)  index: \(indexPath.row)")
                if objData.fieldType == "textfield"  {
                    let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
                    
                if let title = objData.title {
                        cell.txtType.placeholder = title
                    }
                    if let value = objData.fieldVal {
                        cell.txtType.text = value
                    }
                    cell.fieldName = objData.fieldTypeName
                    cell.selectedIndex = indexPath.row
                    cell.delegate = self
                    return cell
                }
                    
                else if objData.fieldType == "select"  {
                    let cell: DropDownCell = tableView.dequeueReusableCell(withIdentifier: "DropDownCell", for: indexPath) as! DropDownCell
                    
                    if let title = objData.title {
                        cell.lblName.text = title
                    }
                    var i = 1
                    for item in objData.values {
                        if item.id == "" {
                            continue
                        }
                        if (defaults.string(forKey: "value") != nil) {
                            if indexPath.row == selectedIndex {
                                let name = UserDefaults.standard.string(forKey: "value")
                                cell.oltPopup.setTitle(name, for: .normal)
                            }
                        }
                        if i == 1 {
                            cell.oltPopup.setTitle(item.name, for: .normal)
                            cell.selectedKey = String(item.id)
                        }
                        i = i + 1
                    }
                    cell.btnPopUpAction = { () in
                        for index in  0..<self.dataArray.count {
                            let cell1  = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? AdPostCell
                            let txtTitle = cell1?.txtType.text
                            if cell1?.fieldName == "ad_title" {
                                if txtTitle == "" {
                                    cell1?.txtType.shake(6, withDelta: 10, speed: 0.06)
                                    let alert = AlertView.prepare(title: "", message:"Please Enter Ad_Title", okAction: {
                                    })
                                    self.presentVC(alert)
                                }
                            }else {
                                cell.dropDownKeysArray = []
                                cell.dropDownValuesArray = []
                                cell.fieldTypeNameArray = []
                                for item in objData.values {
                                    if item.id == "" {
                                        continue
                                    }
                                    cell.dropDownKeysArray.append(String(item.id))
                                    cell.dropDownValuesArray.append(item.name)
                                    cell.fieldTypeNameArray.append(objData.fieldTypeName)
                                }
                                cell.accountDropDown()
                                cell.valueDropDown.show()
                            }
                            self.adForest_populateData()
                            
                        }
                        
                        //                        cell.dropDownKeysArray = []
                        //                        cell.dropDownValuesArray = []
                        //                        cell.fieldTypeNameArray = []
                        //                        for item in objData.values {
                        //                            if item.id == "" {
                        //                                continue
                        //                            }
                        //                            cell.dropDownKeysArray.append(String(item.id))
                        //                            cell.dropDownValuesArray.append(item.name)
                        //                            cell.fieldTypeNameArray.append(objData.fieldTypeName)
                        //                        }
                        //                        cell.accountDropDown()
                        //                        cell.valueDropDown.show()
                    }
                    
                    cell.btnDropdownPopUpAction = { (value,key,param) in
                        
                        let values = value
                        let keys = key
                        let params = param
                        print(values, keys, params)
                        var obj = AdPostField()
                        obj.fieldType = "select"
                        obj.fieldTypeName = params
                        obj.fieldVal = keys
                        self.objArray.append(obj)
                        self.customArray.append(obj)
                        
                    }
                    cell.param = objData.fieldTypeName
                    cell.selectedIndex = indexPath.row
                    //  tableView.reloadData()
                    // cell.delegate = self
                    return cell
                }
                    
                else if objData.fieldType == "textarea" && objData.hasPageNumber == "2"  {
                    let cell: DescriptionTableCell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableCell", for: indexPath) as! DescriptionTableCell
                    
                    if let title = objData.title {
                        cell.lblDescription.text = title
                    }
                    if let value = objData.fieldVal {
                        //cell.richEditorView.html = value
                        cell.txtDescription.text = value
                    }
                    cell.fieldName = objData.fieldTypeName
                    cell.delegate = self
                    return cell
                }
                else if objData.fieldType == "checkbox" {
                    let cell: CheckBoxCell = tableView.dequeueReusableCell(withIdentifier: "CheckBoxCell", for: indexPath) as! CheckBoxCell
                    
                    if let title = objData.title {
                        cell.lblName.text = title
                    }
                    cell.checkvalue = { (value, arr) in
                        let val = value
                        let array = arr
                        print(val,array)
                        var obj = AdPostField()
                        obj.fieldTypeName = val
                        self.objArray.append(obj)
                        self.customArray.append(obj)
                        self.localVariable = ""
                        for item in array {
                            self.localVariable += item + ","
                        }
                        self.localDictionary[cell.fieldName] = self.localVariable
                    }
                    cell.dataArray = objData.values
                    cell.fieldName = objData.fieldTypeName
                    cell.fieldType = objData.fieldType
                    cell.tableView.reloadData()
                    return cell
                }
                else if objData.fieldType == "textfield_url" {
                    let cell: AdPostURLCell = tableView.dequeueReusableCell(withIdentifier: "AdPostURLCell", for: indexPath) as! AdPostURLCell
                    
                    if let placeHolder = objData.title {
                        cell.txtUrl.placeholder = placeHolder
                    }
                    
                    cell.fieldName = objData.fieldTypeName
                    return cell
                }
                    
                else if objData.fieldType == "textfield_date" {
                    let cell: CalendarCell = tableView.dequeueReusableCell(withIdentifier: "CalendarCell", for: indexPath) as! CalendarCell
                    if let title = objData.title {
                        cell.oltDate.setTitle(title, for: .normal)
                    }
                    cell.fieldName = objData.fieldTypeName
//                    cell.datecallback = { (date,name) in
//                        let Name = name
//                        let Date = date
//                        
//                        var obj = AdPostField()
//                        obj.fieldTypeName = Name
//                        obj.fieldVal = Date
//                        self.objArray.append(obj)
//                        self.customArray.append(obj)
//                        
//                    }
                    return cell
                }
            }
            else if section == 4
            {
                let cell: ThirdViewTablecell = tableView.dequeueReusableCell(withIdentifier: "mapCell", for: indexPath) as! ThirdViewTablecell
               
                           cell.btnLocationPopUpAction = { () in
                            cell.locationPopup()
                            cell.locationDropDown.show()
                          
                           }
                cell.passLocations = { (lat,long) in
                    print(lat)
                    print(long)
                    self.latitude = lat
                    self.longitude = long
                    
                }
                cell.oltCheck.addTarget(self, action: #selector(oltCheck), for: .touchUpInside)
                cell.oltPostAdd.addTarget(self, action: #selector(oltPostAdd), for: .touchUpInside)
              //  cell.ChkBtn_Out.addTarget(self, action: #selector(CheckOutBtn), for:.touchUpInside)
                
                if AddsHandler.sharedInstance.objAdPost != nil {
                    let objData = AddsHandler.sharedInstance.objAdPost
                    
                    if let pageTitle = objData?.data.title {
                        self.title = pageTitle
                    }
                    if let infoText = objData?.extra.userInfo {
                        cell.lblUserinfo.text = infoText
                    }
                    if let nameText = objData?.data.profile.name.title {
                        cell.txtName.placeholder = nameText
                    }
                    if  let name = objData?.data.profile.name.fieldVal {
                        cell.txtName.text = name
                    }
                    
                    if let nmbrText = objData?.data.profile.phone.title {
                        cell.txtNumber.placeholder = nmbrText
                    }
                    if let nmbr = objData?.data.profile.phone.fieldVal {
                        cell.txtNumber.text = nmbr
                        self.phone_number = nmbr
                    }
                    
                    var isPhoneEdit = false
                    if let editNumber = objData?.data.profile.phoneEditable {
                        isPhoneEdit = editNumber
                    }
                    
                    if isPhoneEdit {
                        cell.txtNumber.isEnabled = true
                    }
                    else if isPhoneEdit == false {
                        cell.txtNumber.isEnabled = false
                    }
                    
                    if let addressText = objData?.data.profile.location.title {
                        cell.txtAddress.placeholder = addressText
                    }
                    
                    if let addressValue = objData?.data.profile.location.fieldVal {
                        cell.txtAddress.text = addressValue
                        self.address = addressValue
                        if cell.txtAddress.text == "" {
                            print(selectedAddress)
                            cell.txtAddress.text = selectedAddress
                            self.address = selectedAddress
                        }
                    }
                    
                    //guard
                    let isMapShow = objData?.data.profile.map.onOff
                    //                        else {
                    //                        return
                    //                    }
                    
                    if isMapShow! {
                        
                        if let latText = objData?.data.profile.map.locationLat.title {
                            cell.txtLatitude.placeholder = latText
                        }
                        
                        if let latValue =  objData?.data.profile.map.locationLat.fieldVal {
                            cell.txtLatitude.text = latValue
                        }
                        
                        if let longText = objData?.data.profile.map.locationLong.title {
                            cell.txtLongitude.placeholder = longText
                        }
                        
                        if let longValue = objData?.data.profile.map.locationLong.fieldVal {
                            cell.txtLongitude.text = longValue
                        }
                        if let lat = objData?.data.profile.map.locationLat.fieldVal {
                            self.latitude = lat
                        }
                        if let long = objData?.data.profile.map.locationLong.fieldVal {
                            self.longitude = long
                        }
                        
                        if latitude != "" && longitude != "" {
                            initialLocation = CLLocation(latitude: Double(latitude)!, longitude: Double(longitude)!)
                        }
                        cell.centerMapOnLocation(location: initialLocation)
                        cell.addAnnotations(coords: [initialLocation])
                        cell.setupView()
                    }
                        
                    else if isMapShow == false {
                        cell.containerViewMap.isHidden = true
                        cell.containerViewFeatureAdd.translatesAutoresizingMaskIntoConstraints = false
                        cell.containerViewFeatureAdd.topAnchor.constraint(equalTo: cell.containerViewAddress.bottomAnchor, constant: 8).isActive = true
                    }
                    
                    //                    guard
                    let isShowCountry = objData?.data.profile.adCountryShow
                    //else {
                    //                        return
                    //                    }
                    if isShowCountry! {
                        for values in (objData?.data.profile.adCountry.values)! {
                            var i = 1
                            if values.id == "" {
                                continue
                            }
                            cell.popUpArray.append(values.name)
                            cell.hasSubArray.append(values.hasSub)
                            cell.locationIdArray.append(String(values.id))
                            if i == 1 {
                              //  cell.oltPopup.setTitle(values.name, for: .normal)
                            }
                          //  cell.popUpArray = popUpArray
                            i = i + 1
                        }
                        //location popup method call here after asigning data
                        cell.locationPopup()
                    }
                        
                    else if isShowCountry == false {
                        cell.containerViewPopup.isHidden = true
                        cell.containerViewAddress.translatesAutoresizingMaskIntoConstraints = false
                        cell.containerViewAddress.topAnchor.constraint(equalTo: cell.txtNumber.bottomAnchor, constant: 8).isActive = true
                    }
                    
                    if let locationText = objData?.data.profile.adCountry.title {
                        cell.lblLocation.text = locationText
                    }
                    
                    //                    guard
                    let featuredAdBuy = objData?.data.profile.featuredAdBuy
                    //  else {
                    //                        return
                    //                    }
                    
                    if featuredAdBuy! {
                        if let checkButtonTitle = objData?.data.profile.featuredAdNotify.btn {
                            cell.oltCheck.setTitle(checkButtonTitle, for: .normal)
                            cell.oltCheck.backgroundColor = Constants.hexStringToUIColor(hex: "#00a2ff")
                            // imgCheckBox.image = nil
                        }
                        if let featureTitle = objData?.data.profile.featuredAdNotify.text {
                            cell.lblFeatureAdd.text = featureTitle
                        }
                    }
                    
                    //                    guard
                    let featureAdShow = objData?.data.profile.featuredAdIsShow
                    //else {
                    //                        return
                    //                    }
                    
                    if featureAdShow! {
                        cell.imgCheckBox.image = #imageLiteral(resourceName: "uncheck")
                        if let featureAdText = objData?.data.profile.featuredAd.title {
                            cell.lblFeatureAdd.text = featureAdText
                        }
                        
                        if let titlePop = objData?.data.profile.featuredAdText.title {
                            self.popUpTitle = titlePop
                        }
                        if let textPop = objData?.data.profile.featuredAdText.text {
                            self.popUpText = textPop
                        }
                        if let okPop = objData?.data.profile.featuredAdText.btnOk {
                            self.popUpConfirm = okPop
                        }
                        
                        if let cancelPop = objData?.data.profile.featuredAdText.btnNo {
                            self.popUpCancel = cancelPop
                        }
                    }
                    if featureAdShow == false && featuredAdBuy == false {
                        cell.containerViewFeatureAdd.isHidden = true
                    }
                    if let postButtonTitle =  objData?.data.btn_submit_post_ad {
                        cell.oltPostAdd.setTitle(postButtonTitle, for: .normal)
                    }
                    
                    //   guard
                    let isShowBump = objData?.data.profile.bumpAdIsShow
                    //                        else {
                    //                        return
                    //                    }
                    if isShowBump! {
                        cell.imgCheckBump.image = #imageLiteral(resourceName: "uncheck")
                        if let bumpText = objData?.data.profile.bumpAd.title {
                            cell.lblBumpText.text = bumpText
                        }
                    } else {
                        cell.containerViewBumpUpAds.isHidden = true
                        cell.containerViewFeatureAdd.translatesAutoresizingMaskIntoConstraints = false
                        cell.containerViewFeatureAdd.topAnchor.constraint(equalTo: cell.containerViewMap.bottomAnchor, constant: 8).isActive = true
                    }
                }
                
                return cell
            }
            }
            return UITableViewCell()
       }
    
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblCategory
        {
            return 50
        }
        let section = indexPath.section
        var height : CGFloat = 0
        if fieldsArray.count == 0
        {
            if section == 0
            {
                var height: CGFloat = 0
                let objData = dataArray[indexPath.row]
                if objData.fieldType == "textfield" {
                    height = 60
                }
                else if objData.fieldType == "select" {
                    height = 142
                }
                return height
            }
            else if section == 1
            {
                  height = 140
            }
            else if section == 2
            {
                height = 500
            }
            
        }
        else
        {
            if section == 0
            {
                var height: CGFloat = 0
                let objData = dataArray[indexPath.row]
                if objData.fieldType == "textfield" {
                    height = 60
                }
                else if objData.fieldType == "select" {
                    height = 142
                }
                return height
            }
            else if section == 1
            {
                height = 100
            }
            else if section == 2
            {
                if imageArray.isEmpty {
                    height = 0
                } else {
                    height = 140
                }
            }
            else if section == 3
            {
                let objData = fieldsArray[indexPath.row]
                if objData.fieldType == "textarea" {
                    height = 250
                } else if objData.fieldType == "select" {
                    height = 80
                } else if objData.fieldType == "textfield" {
                    height = 80
                } else if objData.fieldType == "checkbox" {
                    height = 230
                } else if objData.fieldType == "textfield_url" {
                    height = 80
                } else if objData.fieldType == "textfield_date" {
                    height = 80
                }
            }
            else if section == 4
            {
                height = 700
            }
        }
        
        return height
        
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if tableView == tblCategory {
//            selectedcategory = dropDownValuesArray[indexPath.row]
//            SelectedCategoryIndex = indexPath.row
//            print(SelectedCategoryIndex)
//            tblCategory.isHidden = true
//            self.tableView.isScrollEnabled = true
//            self.tableView.reloadData()
//        }
//    }
    
    
    func changeTextViewCharacters(value: String, fieldTitle: String) {
        for index in 0..<dataArray.count {
            if let objData = dataArray[index] as? AdPostField {
                                if objData.fieldType == "textarea"
                                {
        var obj = AdPostField()
        obj.fieldType = "textarea"
        obj.fieldVal = value
        obj.fieldTypeName = fieldTitle
        objArray.append(obj)
        
        if fieldTitle == self.dataArray[index].fieldTypeName {
            self.dataArray[index].fieldVal = value
        }
                }
            }
        }
//        for index in 0..<dataArray.count {
//            if let objData = dataArray[index] as? AdPostField {
//                if objData.fieldType == "textarea"
//                {
//                    if (tableView.cellForRow(at: IndexPath(row: index, section: 3)) as? DescriptionTableCell) != nil {
//                        var obj = AdPostField()
//                        obj.fieldType = "textarea"
//                        obj.fieldVal = value
//                        obj.fieldTypeName = fieldTitle
//                        objArray.append(obj)
//
//                        if fieldTitle == self.dataArray[index].fieldTypeName {
//                            self.dataArray[index].fieldVal = value
//                        }
//                    }
//                }
//            }
//        }
    }
    
    //MARK:- API Calls
    func adForest_adPost(param: NSDictionary) {
        print(param)
        self.showLoader()
        AddsHandler.adPost(parameter: param, success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                
                self.title = successResponse.data.title
                AddsHandler.sharedInstance.isCategoeyTempelateOn = successResponse.data.catTemplateOn
                //this ad id send in parameter in 3rd step
                AddsHandler.sharedInstance.adPostAdId = successResponse.data.adId
                AddsHandler.sharedInstance.objAdPost = successResponse
                //Fields
                self.dataArray = successResponse.data.fields
                self.newArray = successResponse.data.fields
                self.imagesArray = successResponse.data.adImages
                for imageId in self.imagesArray {
                    if imageId.imgId == nil {
                        continue
                    }
           //         self.imageIDArray.append(imageId.imgId)
                }
                for item in successResponse.data.fields {
                    if item.hasPageNumber == "1" {
                        self.hasPageNumber = item.hasPageNumber
                    }
                }
                
                //get category id to get dynamic fields
                if let cat_id = successResponse.data.adCatId {
                    self.catID = String(cat_id)
                }
                if successResponse.data.adCatId != nil {
                    let param: [String: Any] = ["cat_id": self.catID,
                                                "ad_id": self.ad_id
                    ]
                    print(param)
                    self.adForest_dynamicFields(param: param as NSDictionary)
                }
                self.tableView.reloadData()
            } else {
                let alert = AlertView.prepare(title: "", message: successResponse.message, okAction: {
                    self.navigationController?.popViewController(animated: true)
                })
                self.presentVC(alert)
            }
        }) { (error) in
            self.stopAnimating()
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        if images.isEmpty {
        }
        else {
            self.photoArray = images
            let param: [String: Any] = [ "ad_id": String(adID)]
            print(param)
            self.adForest_uploadImages(param: param as NSDictionary, images: self.photoArray)
        }
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        self.dismissVC(completion: nil)
    }
    
    // Dynamic Fields
    func adForest_dynamicFields(param: NSDictionary) {
        self.showLoader()
        AddsHandler.adPostDynamicFields(parameter: param, success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                // AddsHandler.sharedInstance.objAdPostData = successResponse.data.fields
                //AddsHandler.sharedInstance.adPostImagesArray = successResponse.data.adImages
            }
            else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            self.stopAnimating()
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    func adForest_uploadImages(param: NSDictionary, images: [UIImage]) {
        print(param)
        print(images)
        self.showLoader()
        DispatchQueue.main.async {
            AddsHandler.adPostUploadImages(parameter: param, imagesArray: images, fileName: "File", uploadProgress: { (uploadProgress) in
            }, success: { (successResponse) in
                print(successResponse)
                self.stopAnimating()
                if successResponse.success {
                    print(successResponse.data.adImages)
                    self.imageArray = successResponse.data.adImages
                    //add image id to array to send to next View Controller and hit to server
                    for item in self.imageArray {
                        self.imageIDArray.append(item.imgId)
                    }
                    self.tableView.reloadData()
                }
                else {
                    let alert = Constants.showBasicAlert(message: successResponse.message)
                    self.presentVC(alert)
                }
            }) { (error) in
                self.stopAnimating()
                let alert = Constants.showBasicAlert(message: error.message)
                self.presentVC(alert)
            }
        }
  
    }
    
//    @objc func CheckOutBtn() {
//        if AddsHandler.sharedInstance.objAdPost != nil {
//            //            let cell = tableView.cellForRow(at: IndexPath(row: 4, section: 4)) as? ThirdViewTablecell
//            let objData = AddsHandler.sharedInstance.objAdPost
//            // cell?.imgCheckBump.image = #imageLiteral(resourceName: "check")
//            var title = ""
//            var message = ""
//            var confirm = ""
//            var cancel = ""
//
//            if let tit = objData?.data.profile.bumpAdText.title {
//                title = tit
//            }
//            if let msg = objData?.data.profile.bumpAdText.text {
//                message = msg
//            }
//            if let con = objData?.data.profile.bumpAdText.btnOk {
//                confirm = con
//            }
//            if let can = objData?.data.profile.bumpAdText.btnNo {
//                cancel = can
//            }
//
//            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//            let confirmAction = UIAlertAction(title: confirm, style: .default) { (action) in
//                //    cell?.imgCheckBump.image = #imageLiteral(resourceName: "check")
//                self.isBump = true
//            }
//            let cancelAction = UIAlertAction(title: cancel, style: .default) { (action) in
//                //  cell?.imgCheckBump.image = #imageLiteral(resourceName: "uncheck")
//            }
//            alert.addAction(cancelAction)
//            alert.addAction(confirmAction)
//            self.presentVC(alert)
//        }
//
//    }
    
    @objc func oltCheck() {
        
        if AddsHandler.sharedInstance.objAdPost != nil {
            
            let objData = AddsHandler.sharedInstance.objAdPost
            guard let isFeatureBuy = objData?.data.profile.featuredAdBuy else {
                return
            }
            if isFeatureBuy {
                if let featureTitle = objData?.data.profile.featuredAdNotify.text {
                    self.showToast(message: featureTitle)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PackagesController") as? PackagesController
                    vc?.isFromAdPost = true
                    //  vc?.delegate = self
                    self.navigationController?.pushViewController(vc!, animated: true)
                    
                } else if isFeatureBuy == false {
                    // self.imgCheckBox.image = #imageLiteral(resourceName: "check")
                    let alert = UIAlertController(title: popUpTitle, message: popUpText, preferredStyle: .alert)
                    let confirm = UIAlertAction(title: popUpConfirm, style: .default) { (action) in
                        //   self.imgCheckBox.image = #imageLiteral(resourceName: "check")
                        self.isFeature = "true"
                    }
                    let cancel = UIAlertAction(title: popUpCancel, style: .default) { (action) in
                        //   self.imgCheckBox.image = #imageLiteral(resourceName: "uncheck")
                    }
                    alert.addAction(cancel)
                    alert.addAction(confirm)
                    self.presentVC(alert)
                }
            }
        }
    }
    
    
//    func onSecondForwardButtonClciked() {
//        localVariable = ""
//        print(fieldsArray)
//        for index in  0..<fieldsArray.count {
//            if let objData = fieldsArray[index] as? AdPostField {
//                if objData.fieldType == "select"  {
//                    if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 3)) as? DropDownCell {
//                        var obj = AdPostField()
//                        obj.fieldType = "select"
//                        obj.fieldTypeName = cell.param
//                        print(cell.param)
//                        obj.fieldVal = cell.selectedKey
//                        objArray.append(obj)
//                        customArray.append(obj)
//                    }
//                }
//                else if objData.fieldType == "textfield"  {
//                    if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 3)) as? TextFieldCell {
//                        var obj = AdPostField()
//                        obj.fieldType = "textfield"
//                        obj.fieldVal = cell.txtType.text
//                        obj.fieldTypeName = cell.fieldName
//                        objArray.append(obj)
//                        customArray.append(obj)
//                    }
//                }
//                else if objData.fieldType == "textarea" {
//                    if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 3)) as? DescriptionTableCell {
//                        var obj = AdPostField()
//                        obj.fieldType = "textarea"
//                        obj.fieldVal = cell.txtDescription.text
//                        obj.fieldTypeName = cell.fieldName
//                        objArray.append(obj)
//                    }
//                }
//                else if objData.fieldType == "checkbox" {
//                    if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 3)) as? CheckBoxCell {
//                        var obj = AdPostField()
//                        obj.fieldTypeName = cell.fieldName
//                        objArray.append(obj)
//                        customArray.append(obj)
//                        localVariable = ""
//                        for item in cell.valueArray {
//                            localVariable += item + ","
//                        }
//                        self.localDictionary[cell.fieldName] = localVariable
//                    }
//                }
//                else if objData.fieldType == "textfield_url" {
//                    if let cell: AdPostURLCell = tableView.cellForRow(at: IndexPath(row: index, section: 3)) as? AdPostURLCell {
//                        var obj = AdPostField()
//                        obj.fieldTypeName = cell.fieldName
//                        guard let txtUrl = cell.txtUrl.text else {return}
//                        if txtUrl.isValidURL {
//                            obj.fieldVal = txtUrl
//                            self.isValidUrl = true
//                        } else {
//                            cell.txtUrl.shake(6, withDelta: 10, speed: 0.06)
//                            self.isValidUrl = false
//                        }
//                        objArray.append(obj)
//                        customArray.append(obj)
//                    }
//                }
//                else if objData.fieldType == "textfield_date" {
//                    if let cell: CalendarCell = tableView.cellForRow(at: IndexPath(row: index, section: 3)) as? CalendarCell {
//                        var obj = AdPostField()
//                        obj.fieldTypeName = cell.fieldName
//                        obj.fieldVal = cell.currentDate
//                        objArray.append(obj)
//                        customArray.append(obj)
//                    }
//                }
//            }
//        }
//
//    }
    
    
    
    @objc func oltPostAdd() {

       // onSecondForwardButtonClciked()
        print(imageIDArray)
        print(latitude)
        print(longitude)
        var parameter: [String: Any] = [
            "images_array": imageIDArray,
            "ad_phone": phone_number,
            "ad_location": selectedAddress,
            "location_lat": latitude,
            "location_long": longitude,
            "ad_country": SelectedCountry,
            "ad_featured_ad": false,
            "ad_id": AddsHandler.sharedInstance.adPostAdId,
            "ad_bump_ad": isBump
        ]
        print(parameter)
        let dataArray = objArray
        
        for (_, value) in dataArray.enumerated() {
            if value.fieldVal == "" {
                continue
            }
            if customArray.contains(where: { $0.fieldTypeName == value.fieldTypeName}) {
                customDictionary[value.fieldTypeName] = value.fieldVal
                print(customDictionary)
            }
            else {
                addInfoDictionary[value.fieldTypeName] = value.fieldVal
                print(addInfoDictionary)
            }
        }
        customDictionary.merge(with: localDictionary)
         print(customDictionary)
        let custom = Constants.json(from: customDictionary)
        if AddsHandler.sharedInstance.isCategoeyTempelateOn {
            let param: [String: Any] = ["custom_fields": custom!]
            print(param)
            parameter.merge(with: param)
        }
        parameter.merge(with: addInfoDictionary)
        print(parameter)
        //self.dummy(param: parameter as NSDictionary)
        self.adForest_postAdthird(param: parameter as NSDictionary)
    }
    
    //////////////////////////////////////////////////////////////
    
    //MARK:- API Call
    //Post Add
    func adForest_postAdthird(param: NSDictionary) {
        self.showLoader()
        AddsHandler.adPostLive(parameter: param, success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                let alert = AlertView.prepare(title: "", message: successResponse.message, okAction: {
                 //   let  CatVC = self.storyboard?.instantiateViewController(withIdentifier: "AddDetailController") as! AddDetailController
                  //  self.navigationController?.pushViewController(CatVC, animated: true)
                self.navigationController?.popToRootViewController(animated: true)
                })
                self.presentVC(alert)
            }
            else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            self.stopAnimating()
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    // sub locations
    func adForest_subLocations(param: NSDictionary) {
        self.showLoader()
        AddsHandler.adPostSubLocations(param: param, success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                AddsHandler.sharedInstance.objSearchCategory = successResponse.data
                let searchCatVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchCategoryDetail") as! SearchCategoryDetail
                searchCatVC.name = self.selectedCountry
                searchCatVC.dataArray = successResponse.data.values
                searchCatVC.modalPresentationStyle = .overCurrentContext
                searchCatVC.modalTransitionStyle = .crossDissolve
                // searchCatVC.delegate = self
                self.presentVC(searchCatVC)
            }
            else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            self.stopAnimating()
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    //MARK:- API Calls
    func adForest_adPostthird(param: NSDictionary) {
        print(param)
        self.showLoader()
        AddsHandler.adPost(parameter: param, success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                
                let success = successResponse
                guard let featuredAdBuy = success.data.profile.featuredAdBuy else {
                    return
                }
                
                if featuredAdBuy {
                    if let checkButtonTitle = success.data.profile.featuredAdNotify.btn {
                        //                        self.oltCheck.setTitle(checkButtonTitle, for: .normal)
                        //                        self.oltCheck.backgroundColor = Constants.hexStringToUIColor(hex: "#00a2ff")
                        //                        self.imgCheckBox.image = nil
                    }
                    if let featureTitle = success.data.profile.featuredAdNotify.text {
                        //  self.lblFeatureAdd.text = featureTitle
                    }
                }
            } else {
                let alert = AlertView.prepare(title: "", message: successResponse.message, okAction: {
                    self.navigationController?.popViewController(animated: true)
                })
                self.presentVC(alert)
            }
        }) { (error) in
            self.stopAnimating()
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
   
}

extension AadPostController: ReturnToAdPostProtocol {
    func Back() {
        let param: [String: Any] = ["is_update": ""]
        print(param)
        self.adForest_adPost(param: param as NSDictionary)
    }
}


