//
//  AddDetailMapCell.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 14/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class AddDetailMapCell: UITableViewCell, GMSMapViewDelegate {

    @IBOutlet var heightConstraintOfMapView: NSLayoutConstraint!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var locTitle: UILabel!
    @IBOutlet var location: UILabel!
    var lat = String()
    var long = String()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setUpView(lat: String, lng: String) {
        let lat : Double = Double(lat) ?? 0.0
        let long : Double = Double(lng) ?? 0.0
       
        let locationValue = CLLocationCoordinate2D(latitude: lat, longitude: long)
        mapView.settings.myLocationButton = false
        mapView.camera = GMSCameraPosition(target: locationValue, zoom: 18 , bearing: 0, viewingAngle: 0)
        
        let marker = GMSMarker()
        marker.position = locationValue
        marker.map = mapView
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


}
