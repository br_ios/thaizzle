//
//  AfterRegistrationVC.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 13/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class AfterRegistrationVC: UIViewController , UITextFieldDelegate ,NVActivityIndicatorViewable {
    
    var phoneNumber = String()

    @IBOutlet weak var lblRegisteration: UILabel!
    @IBOutlet weak var nameTxtFld: UITextField!{
    didSet {
        nameTxtFld.delegate = self
    }
    }
    @IBOutlet weak var emailTxtFlf: UITextField!{
        didSet {
            emailTxtFlf.delegate = self
        }
    }
    @IBOutlet weak var agreeTermAndCondition: UIButton!{
        didSet {
            agreeTermAndCondition.contentHorizontalAlignment = .left
        }
    }
    @IBOutlet weak var paswrdTxtFld: UITextField!{
        didSet {
            paswrdTxtFld.delegate = self
        }
    }
    @IBOutlet weak var phoneNoTxtFld: UITextField!{
        didSet {
            phoneNoTxtFld.delegate = self
            phoneNoTxtFld.text = phoneNumber
        }
    }
    
    @IBOutlet weak var btnTermCondition: UIButton!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBAction func actionCheckBox(_ sender: UIButton) {
        if isAgreeTerms == false {
            btnCheckBox.setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
            isAgreeTerms = true
        }
        else if isAgreeTerms {
            btnCheckBox.setBackgroundImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            isAgreeTerms = false
        }
    }
    
    @IBOutlet weak var buttonRegister: UIButton!{
        didSet {
            buttonRegister.roundCorners()
            buttonRegister.layer.borderColor = UIColor.orange.cgColor
            buttonRegister.layer.borderWidth = 1
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func actionTermCondition(_ sender: Any) {
        
        if self.page_id == "" {
        }
        else {
            let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "TermsConditionsController") as! TermsConditionsController
            termsVC.modalTransitionStyle = .flipHorizontal
            termsVC.modalPresentationStyle = .overCurrentContext
            termsVC.page_id = self.page_id
            self.presentVC(termsVC)
        }
    }
    
    //MARK:- API Calls
    //Get Details Data
    func adForest_registerData() {
        self.showLoader()
        UserHandler.registerDetails(success: { (successResponse) in
            self.stopAnimating()
            print(successResponse)
            if successResponse.success {
                UserHandler.sharedInstance.objregisterDetails = successResponse.data
                if let pageID = successResponse.data.termPageId {
                    self.page_id = pageID
                }
                self.adForest_populateData()
            }
            else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            self.stopAnimating()
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    //MARK:- User Register
    func adForest_registerUser(param: NSDictionary) {
        self.showLoader()
        UserHandler.registerUserAfterVerification(parameter: param, success: { (successResponse) in
            self.stopAnimating()
            print(successResponse)
            if successResponse.success {
               
                self.defaults.set(true, forKey: "isLogin")
                self.defaults.set(self.paswrdTxtFld.text, forKey: "password")
                self.defaults.set(successResponse.data.id, forKey: "user_id")
                 self.defaults.set(successResponse.data.userEmail, forKey: "email")
                self.adForest_EndResponse(PhoneNo: self.phoneNumber, id: successResponse.data.id)
                self.defaults.synchronize()
                
                self.appDelegate.moveToHome()
            }
            else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    func adForest_EndResponse(PhoneNo:String,id:Int){
        UserHandler.Endregister(Phoneno: phoneNumber, id:id, success: { (successResponse) in
          
            print(successResponse)
         
        }) { (error) in
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    @IBAction func actionRegisterBtn(_ sender: UIButton) {
        guard let name = nameTxtFld.text else {
            return
        }
        guard let email = emailTxtFlf.text else {
            return
        }
        guard let phone = phoneNoTxtFld.text else {
            return
        }
        
        guard let password = paswrdTxtFld.text else {
            return
        }
        
        if name == "" {
            self.nameTxtFld.shake(6, withDelta: 10, speed: 0.06)
        }
//        else if email == "" {
//            self.emailTxtFlf.shake(6, withDelta: 10, speed: 0.06)
//        }
//        else if !email.isValidEmail {
//            self.emailTxtFlf.shake(6, withDelta: 10, speed: 0.06)
//        }
            
        else if phone == "" {
            self.phoneNoTxtFld.shake(6, withDelta: 10, speed: 0.06)
        }
       
        else if password == "" {
            self.paswrdTxtFld.shake(6, withDelta: 10, speed: 0.06)
        }
            
        else if isAgreeTerms == false {
            let alert = Constants.showBasicAlert(message: "Please Agree with terms and conditions")
            self.presentVC(alert)
        }
        else {
            let parameters : [String: Any] = [
                "name": name,
                "email": email,
                "phone": phone,
                "password": password
            ]
        
            print(parameters)
            defaults.set(email, forKey: "email")
            defaults.set(password, forKey: "password")
            self.adForest_registerUser(param: parameters as NSDictionary)
        }
    }
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    var isAgreeTerms = false
    var page_id = ""
    var defaults = UserDefaults.standard
    var isVerifivation = false
    var isVerifyOn = false
    
    
    //MARK: - Custom
    func showLoader(){
        self.startAnimating(Constants.activitySize.size, message: Constants.loaderMessages.loadingMessage.rawValue,messageFont: UIFont.systemFont(ofSize: 14), type: NVActivityIndicatorType.ballClipRotatePulse)
    }
    
    
    func adForest_populateData() {
        if UserHandler.sharedInstance.objregisterDetails != nil {
            let objData = UserHandler.sharedInstance.objregisterDetails
            
            if let isVerification = objData?.isVerifyOn {
                self.isVerifyOn = isVerification
            }
            
            if let bgColor = defaults.string(forKey: "mainColor") {
                self.buttonRegister.layer.borderColor = Constants.hexStringToUIColor(hex: bgColor).cgColor
                 self.buttonRegister.backgroundColor = Constants.hexStringToUIColor(hex: bgColor)
                self.buttonRegister.setTitleColor(Constants.hexStringToUIColor(hex: "ffffff"), for: .normal)
            }
            
            if let registerText = objData?.heading {
                self.lblRegisteration.text = registerText
            }
            if let nameText = objData?.namePlaceholder {
                self.nameTxtFld.placeholder = nameText
            }
            if let emailText = objData?.emailPlaceholder {
                self.emailTxtFlf.placeholder = emailText
            }
            if let phoneText = objData?.phonePlaceholder {
                self.phoneNoTxtFld.placeholder = phoneText
            }
            if let passwordtext = objData?.passwordPlaceholder {
                self.paswrdTxtFld.placeholder = passwordtext
            }
            if let termsText = objData?.termsText {
                self.agreeTermAndCondition.setTitle(termsText, for: .normal)
            }
            if let registerText = objData?.formBtn {
                self.buttonRegister.setTitle(registerText, for: .normal)
            }
            
         
            if let isUserVerification = objData?.isVerifyOn {
                self.isVerifivation = isUserVerification
            }
        }
    }
    

    //MARK:- Text Field Delegate Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTxtFld {
            emailTxtFlf.becomeFirstResponder()
        }
        else if textField == emailTxtFlf {
            phoneNoTxtFld.becomeFirstResponder()
        }
        else if textField == phoneNoTxtFld {
            paswrdTxtFld.becomeFirstResponder()
        }
        else if textField == paswrdTxtFld {
            paswrdTxtFld.resignFirstResponder()
        }
        return true
    }
    
    
    
    
    
    
    
    
    
    
        

}
