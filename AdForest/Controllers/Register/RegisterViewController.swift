//
//  RegisterViewController.swift
//  Adforest
//
//  Created by apple on 1/2/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import NVActivityIndicatorView
import SKCountryPicker



class RegisterViewController: UIViewController,UITextFieldDelegate, UIScrollViewDelegate, NVActivityIndicatorViewable, GIDSignInUIDelegate, GIDSignInDelegate  {
    
    //MARK:- Outlets
    
  
    
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var topImage: UIImageView!
    
    
    @IBOutlet weak var countryCodeTxtFld: UITextField!
    @IBOutlet weak var pickerCountry: UIPickerView!
    
    
    @IBOutlet weak var scrollBar: UIScrollView! {
        didSet {
            scrollBar.isScrollEnabled = false
        }
    }
    @IBOutlet weak var lblregisterWithUs: UILabel!

 

  
    @IBOutlet weak var imgPhone: UIImageView!
    @IBOutlet weak var txtPhone: UITextField! {
        didSet {
           
            txtPhone.delegate = self
        }
    }

 
    @IBOutlet weak var buttonRegister: UIButton! {
        didSet {
            buttonRegister.roundCorners()
            buttonRegister.layer.borderWidth = 1
        }
    }
    @IBOutlet weak var lblOr: UILabel!
    @IBOutlet weak var buttonFB: UIButton! {
        didSet {
            buttonFB.roundCorners()
            buttonFB.isHidden = true
        }
    }
    @IBOutlet weak var buttonGoogle: UIButton! {
        didSet {
            buttonGoogle.roundCorners()
            buttonGoogle.isHidden = true
        }
    }
    @IBOutlet weak var buttonAlreadyhaveAccount: UIButton! {
        didSet {
            buttonAlreadyhaveAccount.layer.borderWidth = 0.4
            buttonAlreadyhaveAccount.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    @IBOutlet weak var containerViewSocialButton: UIView!
    
    //MARK:- Properties
    
    var isAgreeTerms = false
    var page_id = ""
    var selectedPhonenumber = ""
    var defaults = UserDefaults.standard
    var isVerifivation = false
    var isVerifyOn = false
    var Phonevariable = String()
    let contryPickerController = CountryPickerController()
    //MARK:- Application Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        countryCodeTxtFld.delegate = self
        self.adForest_registerData()
        lblOr.isHidden = true
        
        if selectedPhonenumber != "" {
            
            btnCountry.setTitle(NSLocale.Key.countryCode.rawValue, for: .normal)
          //  CountryFlagimg.image = imageFlag
            self.txtPhone.text = selectedPhonenumber
        }else{
            let country = CountryManager.shared.currentCountry
            print(country?.dialingCode)
            
            if country?.dialingCode == "+91" {
                btnCountry.setTitle("+66", for: .normal)
            }else {
                btnCountry.setTitle(country?.dialingCode, for: .normal)
            }
            
            btnCountry.clipsToBounds = true
        }
       
    }
    @objc func selectCountryAction(_ sender: Any) {
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
        containerViewSocialButton.isHidden = true
    }
   
    
    //MARK:- Text Field Delegate Methods
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if textField == txtName {
//            txtEmail.becomeFirstResponder()
//        }
//        else if textField == txtEmail {
//            txtPhone.becomeFirstResponder()
//        }
//        else if textField == txtPhone {
//            txtPassword.becomeFirstResponder()
//        }
//        else if textField == txtPassword {
//            txtPassword.resignFirstResponder()
//        }
//        return true
//    }
    
    //MARK: - Custom
    func showLoader(){
        self.startAnimating(Constants.activitySize.size, message: Constants.loaderMessages.loadingMessage.rawValue,messageFont: UIFont.systemFont(ofSize: 14), type: NVActivityIndicatorType.ballClipRotatePulse)
    }

    func adForest_populateData() {
        if UserHandler.sharedInstance.objregisterDetails != nil {
            let objData = UserHandler.sharedInstance.objregisterDetails
            
            if let isVerification = objData?.isVerifyOn {
                self.isVerifyOn = isVerification
            }

            if let bgColor = defaults.string(forKey: "mainColor") {
                self.buttonRegister.layer.borderColor = Constants.hexStringToUIColor(hex: bgColor).cgColor
                 self.buttonRegister.backgroundColor = Constants.hexStringToUIColor(hex: "FF0053")
              //  self.buttonRegister.setTitleColor(Constants.hexStringToUIColor(hex: "ffffff"), for: .normal)
            }
            
            if let registerText = objData?.heading {
                self.lblregisterWithUs.text = registerText
            }
            if let imgUrl = URL(string: (objData?.logo)!) {
                topImage.sd_setShowActivityIndicatorView(true)
                topImage.sd_setIndicatorStyle(.gray)
                topImage.sd_setImage(with: imgUrl, completed: nil)
            }
//            if let nameText = objData?.namePlaceholder {
//                self.txtName.placeholder = nameText
//            }
//            if let emailText = objData?.emailPlaceholder {
//                self.txtEmail.placeholder = emailText
//            }
//            if let phoneText = objData?.phonePlaceholder {
//                self.txtPhone.placeholder = phoneText
//            }
//            if let passwordtext = objData?.passwordPlaceholder {
//                self.txtPassword.placeholder = passwordtext
//            }
//            if let termsText = objData?.termsText {
//                self.buttonAgreeWithTermsConditions.setTitle(termsText, for: .normal)
//            }
            if let registerText = objData?.formBtn {
                self.buttonRegister.setTitle("Send", for: .normal)
            }

            if let loginText = objData?.loginText {
                self.buttonAlreadyhaveAccount.setTitle(loginText, for: .normal)
            }
            if let isUserVerification = objData?.isVerifyOn {
                self.isVerifivation = isUserVerification
            }
            
            // Show hide guest button
            guard let settings = defaults.object(forKey: "settings") else {
                return
            }
            let  settingObject = NSKeyedUnarchiver.unarchiveObject(with: settings as! Data) as! [String : Any]
            let objSettings = SettingsRoot(fromDictionary: settingObject)
            
            // Show/hide google and facebook button
            var isShowGoogle = false
            var isShowFacebook = false
            
            if let isGoogle = objSettings.data.registerBtnShow.google {
                isShowGoogle = isGoogle
            }
            if let isFacebook = objSettings.data.registerBtnShow.facebook{
                isShowFacebook = isFacebook
            }
            if isShowFacebook || isShowGoogle {
                if let sepratorText = objData?.separator {
                    self.lblOr.text = sepratorText
                }
            }
            
            if isShowFacebook && isShowGoogle {
                self.buttonFB.isHidden = true
                self.buttonGoogle.isHidden = true
                if let fbText = objData?.facebookBtn {
                    self.buttonFB.setTitle(fbText, for: .normal)
                }
                if let googletext = objData?.googleBtn {
                    self.buttonGoogle.setTitle(googletext, for: .normal)
                }
            }
                
            else if isShowFacebook && isShowGoogle == true {
                self.buttonFB.isHidden = false
                self.buttonFB.translatesAutoresizingMaskIntoConstraints = false
                buttonFB.leftAnchor.constraint(equalTo: self.containerViewSocialButton.leftAnchor, constant: 0).isActive = true
                buttonFB.rightAnchor.constraint(equalTo: self.containerViewSocialButton.rightAnchor, constant: 0).isActive = true
                if let fbText = objData?.facebookBtn {
                    self.buttonFB.setTitle(fbText, for: .normal)
                }
            }
                
            else if isShowGoogle && isShowFacebook == true {
                self.buttonGoogle.isHidden = false
                self.buttonGoogle.translatesAutoresizingMaskIntoConstraints = false
                buttonGoogle.leftAnchor.constraint(equalTo: self.containerViewSocialButton.leftAnchor, constant: 0).isActive = true
                buttonGoogle.rightAnchor.constraint(equalTo: self.containerViewSocialButton.rightAnchor, constant: 0).isActive = true
                
                if let googletext = objData?.googleBtn {
                    self.buttonGoogle.setTitle(googletext, for: .normal)
                }
            }
        }
    }
    
    //MARK: -IBActions
    
//    @IBAction func checkBox(_ sender: UIButton) {
//
//        if isAgreeTerms == false {
//            buttonCheckBox.setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
//            isAgreeTerms = true
//        }
//        else if isAgreeTerms {
//            buttonCheckBox.setBackgroundImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
//            isAgreeTerms = false
//        }
//    }
//
//    @IBAction func actionTermsCondition(_ sender: UIButton) {
//        if self.page_id == "" {
//        }
//        else {
//            let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "TermsConditionsController") as! TermsConditionsController
//            termsVC.modalTransitionStyle = .flipHorizontal
//            termsVC.modalPresentationStyle = .overCurrentContext
//            termsVC.page_id = self.page_id
//            self.presentVC(termsVC)
//        }
//    }
    
    @IBAction func handlebck(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionRegister(_ sender: UIButton) {
        
        view.endEditing(true)
        guard let phone = txtPhone.text else {
            return
        }
        guard let countryCode = btnCountry.titleLabel!.text else {
            return
        }
//
         if phone == "" {
             self.txtPhone.shake(6, withDelta: 10, speed: 0.06)
        }
        else if !phone.isValidPhone {
             self.txtPhone.shake(6, withDelta: 10, speed: 0.06)
        }
            
        else  {
            let phoneWithCountryCode = "\(countryCode)" + "\(phone)"
            Phonevariable = phoneWithCountryCode
          
             let params = ["action":"sb_verification_system2","sb_phone_numer":phoneWithCountryCode]
            self.PhoneNumberApi(Phoneno: phoneWithCountryCode)
           
           // self.adForest_registerUser(param: params as NSDictionary)
        }
    }
    
    @IBAction func actionCountryCode(_ sender: Any) {
        let countryController = (CountryPickerWithSectionViewController).presentController(on: self) { (country: Country) in
            //            self.CountryFlagimg.image = country.flag
                    self.btnCountry.setTitle(country.dialingCode, for: .normal)
            //
            }
    }
    @IBAction func actionFacebook(_ sender: Any) {
        let loginManager = FBSDKLoginManager()
        
        loginManager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            if error != nil {
                print(error?.localizedDescription ?? "Nothing")
            }
            else if (result?.isCancelled)! {
                print("Cancel")
            }
            else if error == nil {
                self.userProfileDetails()
            } else {
            }
        }
    }
    
//    @IBAction func actionGoogle(_ sender: Any) {
//        if GoogleAuthenctication.isLooggedIn {
//            GoogleAuthenctication.signOut()
//        }
//        else {
//            GoogleAuthenctication.signIn()
//        }
//    }
    
    @IBAction func actionLoginHere(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func PhoneNumberApi(Phoneno: String) {
      
         //   self.showLoader()
            UserHandler.registerOtp(Phoneno: Phoneno, success: { (successResponse) in
                self.stopAnimating()
                if successResponse.success != true {
                   let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyCodeVC") as! VerifyCodeVC
                    vc.Phonenumber = self.Phonevariable
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else {
                    
                    let alert = Constants.showBasicAlert(message: successResponse.message)
                    self.presentVC(alert)
                }
            }) { (error) in
                
             //   let alert = Constants.showBasicAlert(message: error.message)
             //   self.presentVC(alert)
              
            }
        }
    
    //MARK:- Facebook Delegate Methods
    
    func userProfileDetails() {
        if (FBSDKAccessToken.current() != nil) {
            FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, first_name, last_name, email, gender, picture.type(large)"]).start { (connection, result, error) in
                if error != nil {
                    print(error?.localizedDescription ?? "Nothing")
                    return
                }
                else {
                    guard let results = result as? NSDictionary else { return }
                     guard  let email = results["email"] as? String else {
                        return
                    }
                    let param: [String: Any] = [
                        "email": email,
                        "type": "social"
                    ]
                    print(param)
                    self.defaults.set(true, forKey: "isSocial")
                    self.defaults.set(email, forKey: "email")
                    self.defaults.set("1122", forKey: "password")
                    self.defaults.synchronize()
                    self.adForest_loginUser(parameters: param as NSDictionary)
                }
            }
        }
    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
    
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
    //MARK:- Google Delegate Methods
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
        }
        if error == nil {
            guard let email = user.profile.email else { return }
            let param: [String: Any] = [
                "email": email,
                "type": "social"
            ]
            print(param)
            self.defaults.set(true, forKey: "isSocial")
            self.defaults.set(email, forKey: "email")
            self.defaults.set("1122", forKey: "password")
            self.defaults.synchronize()
            self.adForest_loginUser(parameters: param as NSDictionary)
        }
    }
    // Google Sign In Delegate
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- API Calls
    //Get Details Data
    func adForest_registerData() {
        self.showLoader()
        UserHandler.registerDetails(success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                UserHandler.sharedInstance.objregisterDetails = successResponse.data
                if let pageID = successResponse.data.termPageId {
                    self.page_id = pageID
                }
                self.adForest_populateData()
            }
            else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            self.stopAnimating()
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    //MARK:- User Register
    func adForest_registerUser(param: NSDictionary) {
        self.showLoader()
        UserHandler.registerUser(parameter: param, success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                if self.isVerifivation {
                    let alert = AlertView.prepare(title: "", message: successResponse.message, okAction: {
                        let confirmationVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
                        confirmationVC.isFromVerification = true
                        confirmationVC.phoneNumber = successResponse.data.phone
                        
                        confirmationVC.verificationCode = successResponse.data.code
                        self.navigationController?.pushViewController(confirmationVC, animated: true)
                    })
                   self.presentVC(alert)
                }
                else {
                    self.defaults.set(true, forKey: "isLogin")
                    self.defaults.synchronize()
                    self.appDelegate.moveToHome()
                }
            }
            else {
                
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    // Login User With Social
    func adForest_loginUser(parameters: NSDictionary) {
        self.showLoader()
        UserHandler.loginUser(parameter: parameters , success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success{
                if self.isVerifyOn && successResponse.data.isAccountConfirm == false {
                    let alert = AlertView.prepare(title: "", message: successResponse.message, okAction: {
                        let confirmationVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
                        confirmationVC.isFromVerification = true
                        confirmationVC.user_id = successResponse.data.id
                        self.navigationController?.pushViewController(confirmationVC, animated: true)
                    })
                    self.presentVC(alert)
                }
                else {
                    self.defaults.set(true, forKey: "isLogin")
                    self.defaults.synchronize()
                    self.defaults.set(successResponse.data.id, forKey: "user_id")
                    self.appDelegate.moveToHome()
                }
            }
            else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
}





