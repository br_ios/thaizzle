//  ViewController.swift
//  AdForest
//  Created by Siddharth Awasthi on 13/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import LineSDK
import NVActivityIndicatorView
import SKCountryPicker

class RegViewController: UIViewController,NVActivityIndicatorViewable, GIDSignInUIDelegate, GIDSignInDelegate  {
    
     var defaults = UserDefaults.standard
    
    @IBOutlet var mainView: UIView!
    @IBOutlet var numberTextField: UITextField!
    @IBOutlet var smsTextField: UITextField!
    @IBOutlet var oltSendBtn: UIButton!
    @IBOutlet var oltNextBtn: UIButton!
    @IBOutlet var oltLogin: UILabel!
    @IBOutlet var oltMobileNo: UILabel!
    @IBOutlet var oltSmsCode: UILabel!
    @IBOutlet var oltConnectOthrWays: UILabel!
    @IBOutlet weak var btnCountry: UIButton!
    
    var isVerifyOn = false
    var oTpCode = Int()
    var isOtPCodeSend: Bool = false
    var resendText = String()
    var selectedPhonenumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
      numberTextField.delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
         btnCountry.setTitle("+66", for: .normal)
        if selectedPhonenumber != "" {
            
            btnCountry.setTitle(NSLocale.Key.countryCode.rawValue, for: .normal)
            //  CountryFlagimg.image = imageFlag
            self.numberTextField.text = selectedPhonenumber
        }else{
            let country = CountryManager.shared.currentCountry
            print(country)
            if country?.dialingCode! == "+91" {
                btnCountry.setTitle("+66", for: .normal)
            }else {
                btnCountry.setTitle(country?.dialingCode, for: .normal)
            }
          //  btnCountry.setTitle(country?.dialingCode, for: .normal)
           
            
            btnCountry.clipsToBounds = true
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adForest_loginDetails()
        self.navigationController?.isNavigationBarHidden = true
         oltBackBtn.setImage(UIImage(named: "backbuttonLogin"), for: .normal)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBOutlet var oltBackBtn: UIButton!
    
    @IBAction func actnBackBtn(_ sender: UIButton) {
         navigationController?.popViewController(animated: true)
    }
    @IBAction func actnSmsCodeSendBtn(_ sender: UIButton) {
        adForest_logIn()
    }
    
    @IBAction func actnLoginBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionCountryCode(_ sender: Any) {
        let countryController = (CountryPickerWithSectionViewController).presentController(on: self) { (country: Country) in
            //            self.CountryFlagimg.image = country.flag
            self.btnCountry.setTitle(country.dialingCode, for: .normal)
            //
        }
    }
    
    @IBAction func actnNextBtn(_ sender: UIButton) {
        if self.smsTextField.text != "" {
            if self.smsTextField.text! == String(self.oTpCode) {
                let vc = storyboard?.instantiateViewController(withIdentifier: "AfterRegistrationVC") as! AfterRegistrationVC
                vc.phoneNumber =  btnCountry.titleLabel!.text! + numberTextField.text!
                self.navigationController?.pushViewController(vc, animated: true)
            } else{
                let alert = Constants.showBasicAlert(message: "Incorrect OTP entered.")
                self.presentVC(alert)
            }
        } else {
            let alert = Constants.showBasicAlert(message: "Please enter OTP.")
            self.presentVC(alert)
        }
    }
    
    @IBAction func actnFacebook(_ sender: UIButton) {
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            if error != nil {
                print(error?.localizedDescription ?? "Nothing")
            }
            else if (result?.isCancelled)! {
                print("Cancel")
            }
            else if error == nil {
                self.userProfileDetails()
            } else {
            }
        }
    }
    
    //MARK:- Facebook Delegate Methods
    func userProfileDetails() {
        
        if (FBSDKAccessToken.current() != nil) {
            self.showLoader()
            FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, first_name, last_name, email, gender, picture.type(large)"]).start { (connection, result, error) in
                self.stopAnimating()
                if error != nil {
                    print(error?.localizedDescription ?? "Nothing")
                    return
                }
                else {
                    guard let results = result as? NSDictionary else { return }
                    guard let facebookId = results["id"] as? String else {
                        return
                    }
                    var email = results["email"] as? String ?? ""
                    let name = results["name"] as? String ?? ""
                    if email == "" {
                        email = facebookId + "@facebook.com"
                    }
                    print("\(email), \(facebookId)")
                    let param: [String: Any] = [
                        "email": email,
                        "user_name" : name,
                        "social_id" : facebookId,
                        "type": "social"
                    ]
                    print(param)
                    self.defaults.set(true, forKey: "isSocial")
                    self.defaults.set(facebookId, forKey: "social_id")
                    self.defaults.set(email, forKey: "email")
                    self.defaults.set("1122", forKey: "password")
                    self.defaults.synchronize()
                    
                  self.adForest_loginUser(parameters: param as NSDictionary)
                }
            }
        }
    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
    
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        
        return true
    }
    
    @IBAction func actnLine(_ sender: UIButton) {
        
        LoginManager.shared.login(permissions: [.profile], in: self) {
            result in
            switch result {
            case .success(let loginResult):
                print(loginResult.accessToken.value)
                var email = ""
                if   let user_id = loginResult.userProfile?.userID {
                    email = user_id + "@line.com"
                }
                var userName = ""
                if  let username = loginResult.userProfile?.displayName {
                    userName = username
                }
                var id = ""
                if  let ido = loginResult.userProfile?.userID {
                    id = ido
                }
                let param: [String: Any] = [
                    "email": email,
                    "user_name" : userName,
                    "social_id" : id,
                    "type": "social"
                    
                ]
                print(param)
                self.defaults.set(true, forKey: "isSocial")
                self.defaults.set(email, forKey: "email")
                self.defaults.set(id, forKey: "social_id")
                self.defaults.set("1122", forKey: "password")
                self.defaults.synchronize()
                self.adForest_loginUser(parameters: param as NSDictionary)
                break;
                
            // Do other things you need with the login result
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func actnGoogle(_ sender: UIButton) {
        if GoogleAuthenctication.isLooggedIn {
            GoogleAuthenctication.signOut()
        }
        else {
            GoogleAuthenctication.signIn()
       }
       // GIDSignIn.sharedInstance().signIn()
    }
    
    //MARK:- Google Delegate Methods
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
        }
        if error == nil {
            guard let googleID = user.userID,
                let name = user.profile.name
                else { return }
            guard let token = user.authentication.idToken else {
                return
            }
            var email = user.profile.email ?? ""
            if email == "" {
            email = googleID + "@gmail.com"
            }
          print("\(email), \(googleID), \(name), \(token)")
            let param: [String: Any] = [
                "email": email,
                "user_name": name,
                "social_id" : googleID,
                "type": "social"
            ]
            print(param)
            self.defaults.set(true, forKey: "isSocial")
            self.defaults.set(email, forKey: "email")
            self.defaults.set(googleID, forKey: "social_id")
            self.defaults.set("1122", forKey: "password")
            self.defaults.synchronize()
            self.adForest_loginUser(parameters: param as NSDictionary)
        }
    }
    // Google Sign In Delegate
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == numberTextField {
            smsTextField.becomeFirstResponder()
        }
        else if textField == smsTextField {
            smsTextField.resignFirstResponder()
        }
        return true
    }
    func showLoader(){
        self.startAnimating(Constants.activitySize.size, message: Constants.loaderMessages.loadingMessage.rawValue,messageFont: UIFont.systemFont(ofSize: 14), type: NVActivityIndicatorType.ballClipRotatePulse)
    }
    
    func adForest_logIn() {
        let number = numberTextField.text
        if number == "" {
            self.numberTextField.shake(6, withDelta: 10, speed: 0.06)
        } else  {
            let phoneWithCountryCode = (btnCountry.titleLabel?.text!)!  + (number ?? "")!
            let parameters : [String: Any] = [
                "phone": phoneWithCountryCode
                
            ]
            print(parameters)
            self.adForest_registerUser(param: parameters as NSDictionary)
        }
      
    }
    func adForest_registerUser(param: NSDictionary) {
        self.showLoader()
        UserHandler.registerUser(parameter: param, success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                print(successResponse)
                print(successResponse.data.code)
                self.oTpCode = successResponse.data.code
                self.oltSendBtn.setTitle(self.resendText, for: .normal)
                
            } else {
                
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            self.stopAnimating()
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    // Login User
    func adForest_loginUser(parameters: NSDictionary) {
        self.showLoader()
        UserHandler.loginUser(parameter: parameters , success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                self.defaults.set(true, forKey: "isLogin")
                self.defaults.set(successResponse.data.id, forKey: "user_id")
                self.defaults.synchronize()
                self.appDelegate.moveToHome()
            } else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    func adForest_loginDetails() {
        self.showLoader()
        UserHandler.loginDetails(success: { (successResponse) in
            self.stopAnimating()
            if successResponse.success {
                UserHandler.sharedInstance.objLoginDetails = successResponse.data
                self.adForest_populateData()
            } else {
                let alert = Constants.showBasicAlert(message: successResponse.message)
                self.presentVC(alert)
            }
        }) { (error) in
            self.stopAnimating()
            let alert = Constants.showBasicAlert(message: error.message)
            self.presentVC(alert)
        }
    }
    
    func adForest_populateData() {
        if UserHandler.sharedInstance.objLoginDetails != nil {
            let objData = UserHandler.sharedInstance.objLoginDetails
            
            if let isVerification = objData?.isVerifyOn {
                self.isVerifyOn = isVerification
            }
            
            if let bgColor = defaults.string(forKey: "mainColor") {
                //self.containerViewImage.backgroundColor = Constants.hexStringToUIColor(hex: bgColor)
                //self.buttonSubmit.layer.borderColor = Constants.hexStringToUIColor(hex: bgColor).cgColor
                //self.buttonSubmit.backgroundColor = Constants.hexStringToUIColor(hex: bgColor)
                //self.buttonGuestLogin.layer.borderColor = Constants.hexStringToUIColor(hex: bgColor).cgColor
                // self.buttonSubmit.setTitleColor(Constants.hexStringToUIColor(hex: "ffffff"), for: .normal)
                
                //self.buttonGuestLogin.setTitleColor(Constants.hexStringToUIColor(hex: bgColor), for: .normal)
            }
            
            //            if let imgUrl = URL(string: (objData?.logo)!) {
            //                imgTitle.sd_setShowActivityIndicatorView(true)
            //                imgTitle.sd_setIndicatorStyle(.gray)
            //                imgTitle.sd_setImage(with: imgUrl, completed: nil)
            //            }
            //
            //            if let welcomeText = objData?.heading {
            //                self.lblWelcome.text = welcomeText
            //            }
            if let mobileNumber = objData?.mobile_number {
                self.oltMobileNo.text = mobileNumber
            }
            
            if let smsCode = objData?.sms_code {
                self.oltSmsCode.text = smsCode
            }
            
            if let next = objData?.next {
                self.oltNextBtn.setTitle(next, for: .normal)
            }
            
            if let send = objData?.send {
                 self.oltSendBtn.setTitle(send, for: .normal)
            }
            if let resend = objData?.resend {
                self.resendText = resend
            }
            
            if let connectOtherWays = objData?.connect_with_other_ways {
                self.oltConnectOthrWays.text = connectOtherWays
            }
           
            
            if let login = objData?.login {
                self.oltLogin.text = login
            }
        }
    }
}

extension RegViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if textField == numberTextField{
            if (numberTextField.text?.contains("+"))!{
                guard let valuetxt = numberTextField.text else { return true}
                var txtfieldtxt: String = valuetxt
                txtfieldtxt.remove(at: txtfieldtxt.index(before: txtfieldtxt.endIndex))
                textField.text = txtfieldtxt
            }
    }
    return true
   }
}


/*
 //
 //  AadPostController.swift
 //  AdForest
 //
 //  Created by apple on 4/25/18.
 //  Copyright © 2018 apple. All rights reserved.
 //
 
 import UIKit
 import NVActivityIndicatorView
 
 class AadPostController: UIViewController, NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource, textFieldValueDelegate, PopupValueChangeDelegate {
 
 //MARK:- Outlets
 @IBOutlet weak var tableView: UITableView! {
 didSet {
 tableView.delegate = self
 tableView.dataSource = self
 tableView.tableFooterView = UIView()
 tableView.separatorStyle = .singleLine
 tableView.separatorColor = UIColor.black
 tableView.showsVerticalScrollIndicator = false
 }
 }
 
 //MARK:- Properties
 var isFromEditAd = false
 var ad_id = 0
 var catID = ""
 var dataArray = [AdPostField]()
 var newArray = [AdPostField]()
 var imagesArray = [AdPostImageArray]()
 var dynamicArray = [AdPostField]()
 var hasPageNumber = ""
 var refreshArray = [AdPostField]()
 var imageIDArray = [Int]()
 var data = [AdPostField]()
 let defaults = UserDefaults.standard
 var selectTxt = String()
 
 // Empty Fields Check
 var adTitle = ""
 
 //MARK:- View Life Cycle
 
 override func viewDidLoad() {
 super.viewDidLoad()
 self.showBackButton()
 self.forwardButton()
 self.googleAnalytics(controllerName: "Add Post Controller")
 if isFromEditAd {
 let param: [String: Any] = ["is_update": ad_id]
 print(param)
 self.adForest_adPost(param: param as NSDictionary)
 }
 else {
 let param: [String: Any] = ["is_update": ""]
 print(param)
 self.adForest_adPost(param: param as NSDictionary)
 }
 }
 
 //MARK: - Custom
 func showLoader() {
 self.startAnimating(Constants.activitySize.size, message: Constants.loaderMessages.loadingMessage.rawValue,messageFont: UIFont.systemFont(ofSize: 14), type: NVActivityIndicatorType.ballClipRotatePulse)
 }
 
 func forwardButton() {
 let button = UIButton(type: .custom)
 if #available(iOS 11, *) {
 button.widthAnchor.constraint(equalToConstant: 30).isActive = true
 button.heightAnchor.constraint(equalToConstant: 30).isActive = true
 }
 else {
 button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
 }
 if defaults.bool(forKey: "isRtl") {
 button.setBackgroundImage(#imageLiteral(resourceName: "backbutton"), for: .normal)
 } else {
 button.setBackgroundImage(#imageLiteral(resourceName: "forwardButton"), for: .normal)
 }
 button.addTarget(self, action: #selector(onForwardButtonClciked), for: .touchUpInside)
 let forwardBarButton = UIBarButtonItem(customView: button)
 self.navigationItem.rightBarButtonItem = forwardBarButton
 }
 
 func changeText(value: String, fieldTitle: String) {
 for index in 0..<dataArray.count {
 if let objData = dataArray[index] as? AdPostField {
 if objData.fieldType == "textfield" {
 if let cell  = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? AdPostCell {
 var obj = AdPostField()
 obj.fieldVal = value
 obj.fieldTypeName = cell.fieldName
 obj.fieldType = "textfield"
 data.append(obj)
 if fieldTitle == self.dataArray[index].fieldTypeName {
 self.dataArray[index].fieldVal = value
 }
 }
 }
 }
 }
 }
 
 func changePopupValue(selectedKey: String, fieldTitle: String, selectedText: String) {
 print(selectedKey, fieldTitle, selectedText)
 self.selectTxt = selectedText
 
 for index in 0..<dataArray.count {
 if let objData = dataArray[index] as? AdPostField {
 if objData.fieldType == "select" {
 if let cell  = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? AdPostPopupCell {
 var obj = AdPostField()
 obj.fieldVal = selectedKey
 obj.fieldTypeName = fieldTitle
 obj.fieldType = "select"
 data.append(obj)
 
 if fieldTitle == self.dataArray[index].fieldTypeName {
 self.dataArray[index].fieldVal = selectedText
 cell.oltPopup.setTitle(selectedText, for: .normal)
 }
 }
 }
 }
 }
 }
 
 
 @objc func onForwardButtonClciked() {
 var data = [AdPostField]()
 for index in  0..<dataArray.count {
 if let objData = dataArray[index] as? AdPostField {
 if objData.fieldType == "textfield" {
 if let cell  = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? AdPostCell {
 var obj = AdPostField()
 obj.fieldVal = cell.txtType.text
 obj.fieldTypeName = cell.fieldName
 obj.fieldType = "textfield"
 data.append(obj)
 
 guard let txtTitle = cell.txtType.text else {return}
 if cell.fieldName == "ad_title" {
 if txtTitle == "" {
 cell.txtType.shake(6, withDelta: 10, speed: 0.06)
 } else {
 self.adTitle = txtTitle
 }
 }
 
 }
 }
 else if objData.fieldType == "select" {
 if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? AdPostPopupCell {
 var obj = AdPostField()
 obj.fieldVal = cell.selectedKey
 obj.fieldTypeName = cell.fieldName
 obj.fieldType = "select"
 print(obj.fieldVal, obj.fieldTypeName, obj.fieldType)
 data.append(obj)
 }
 }
 }
 }
 if self.adTitle == "" {
 
 }
 else {
 let postVC = self.storyboard?.instantiateViewController(withIdentifier: "AdPostImagesController") as! AdPostImagesController
 if AddsHandler.sharedInstance.isCategoeyTempelateOn {
 self.refreshArray = dataArray
 self.refreshArray.insert(contentsOf: AddsHandler.sharedInstance.objAdPostData, at: 2)
 postVC.fieldsArray = self.refreshArray
 postVC.objArray = data
 postVC.isfromEditAd = self.isFromEditAd
 }
 else {
 postVC.objArray = data
 postVC.fieldsArray = self.dataArray
 postVC.isfromEditAd = self.isFromEditAd
 }
 postVC.imageArray = self.imagesArray
 postVC.imageIDArray = self.imageIDArray
 self.navigationController?.pushViewController(postVC, animated: true)
 }
 }
 
 //MARK:- Table View Delegate Methods
 
 func numberOfSections(in tableView: UITableView) -> Int {
 return 1
 }
 
 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 if hasPageNumber == "1" {
 return dataArray.count
 }
 return dataArray.count
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 let objData = dataArray[indexPath.row]
 if objData.fieldType == "textfield"  && objData.hasPageNumber == "1" {
 let cell = tableView.dequeueReusableCell(withIdentifier: "AdPostCell", for: indexPath) as! AdPostCell
 
 if let title = objData.title  {
 cell.txtType.placeholder = title
 }
 if let fieldValue = objData.fieldVal {
 cell.txtType.text = fieldValue
 }
 cell.fieldName = objData.fieldTypeName
 cell.delegateText = self
 return cell
 }
 else if objData.fieldType == "select" && objData.hasPageNumber == "1"  {
 let cell: AdPostPopupCell = tableView.dequeueReusableCell(withIdentifier: "AdPostPopupCell", for: indexPath) as! AdPostPopupCell
 
 if let title = objData.title {
 cell.lblType.text = title
 
 }
 
 
 if let fieldValue = objData.fieldVal {
 cell.oltPopup.setTitle(fieldValue, for: .normal)
 }
 var i = 1
 for item in objData.values {
 if item.id == "" {
 continue
 }
 if i == 1 {
 cell.oltPopup.setTitle(item.name, for: .normal)
 cell.selectedKey = String(item.id)
 }
 i = i + 1
 }
 
 cell.btnPopUpAction = { () in
 cell.dropDownKeysArray = []
 cell.dropDownValuesArray = []
 cell.hasCatTemplateArray = []
 cell.hasTempelateArray = []
 cell.hasSubArray = []
 for items in objData.values {
 if items.id == "" {
 continue
 }
 cell.dropDownKeysArray.append(String(items.id))
 cell.dropDownValuesArray.append(items.name)
 cell.dropDownValuesArray.append(items.ImageDrop)
 
 cell.hasCatTemplateArray.append(objData.hasCatTemplate)
 cell.hasTempelateArray.append(items.hasTemplate)
 cell.hasSubArray.append(items.hasSub)
 }
 cell.popupShow()
 cell.selectionDropdown.show()
 }
 cell.fieldName = objData.fieldTypeName
 cell.delegatePopup = self
 return cell
 }
 return UITableViewCell()
 }
 
 func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
 var height: CGFloat = 0
 let objData = dataArray[indexPath.row]
 if objData.fieldType == "textfield" {
 height = 60
 }
 else if objData.fieldType == "select" {
 height = 80
 }
 return height
 }
 
 //MARK:- API Calls
 func adForest_adPost(param: NSDictionary) {
 print(param)
 self.showLoader()
 AddsHandler.adPost(parameter: param, success: { (successResponse) in
 self.stopAnimating()
 if successResponse.success {
 print(successResponse)
 self.title = successResponse.data.title
 AddsHandler.sharedInstance.isCategoeyTempelateOn = successResponse.data.catTemplateOn
 //this ad id send in parameter in 3rd step
 AddsHandler.sharedInstance.adPostAdId = successResponse.data.adId
 AddsHandler.sharedInstance.objAdPost = successResponse
 //Fields
 self.dataArray = successResponse.data.fields
 self.newArray = successResponse.data.fields
 self.imagesArray = successResponse.data.adImages
 for imageId in self.imagesArray {
 if imageId.imgId == nil {
 continue
 }
 self.imageIDArray.append(imageId.imgId)
 }
 for item in successResponse.data.fields {
 if item.hasPageNumber == "1" {
 self.hasPageNumber = item.hasPageNumber
 }
 }
 
 //get category id to get dynamic fields
 if let cat_id = successResponse.data.adCatId {
 self.catID = String(cat_id)
 }
 if successResponse.data.adCatId != nil {
 let param: [String: Any] = ["cat_id": self.catID,
 "ad_id": self.ad_id
 ]
 print(param)
 self.adForest_dynamicFields(param: param as NSDictionary)
 }
 self.tableView.reloadData()
 } else {
 let alert = AlertView.prepare(title: "", message: successResponse.message, okAction: {
 self.navigationController?.popViewController(animated: true)
 })
 self.presentVC(alert)
 }
 }) { (error) in
 self.stopAnimating()
 let alert = Constants.showBasicAlert(message: error.message)
 self.presentVC(alert)
 }
 }
 
 // Dynamic Fields
 func adForest_dynamicFields(param: NSDictionary) {
 self.showLoader()
 AddsHandler.adPostDynamicFields(parameter: param, success: { (successResponse) in
 self.stopAnimating()
 if successResponse.success {
 // AddsHandler.sharedInstance.objAdPostData = successResponse.data.fields
 //AddsHandler.sharedInstance.adPostImagesArray = successResponse.data.adImages
 }
 else {
 let alert = Constants.showBasicAlert(message: successResponse.message)
 self.presentVC(alert)
 }
 }) { (error) in
 self.stopAnimating()
 let alert = Constants.showBasicAlert(message: error.message)
 self.presentVC(alert)
 }
 }
 }

 
 
 */

///////////




/*
 //
 //  AdPostCell.swift
 //  AdForest
 //
 //  Created by apple on 4/25/18.
 //  Copyright © 2018 apple. All rights reserved.
 //
 
 import UIKit
 import TextFieldEffects
 import DropDown
 import NVActivityIndicatorView
 
 protocol textFieldValueDelegate {
 func changeText(value: String, fieldTitle: String)
 }
 
 protocol PopupValueChangeDelegate {
 func changePopupValue(selectedKey: String, fieldTitle: String, selectedText : String)
 }
 
 class AdPostCell: UITableViewCell , UITextFieldDelegate {
 
 //MARK:- Outlets
 @IBOutlet weak var containerView: UIView!
 @IBOutlet weak var txtType: HoshiTextField! {
 didSet {
 txtType.delegate = self
 if let mainColor = defaults.string(forKey: "mainColor") {
 txtType.borderActiveColor = Constants.hexStringToUIColor(hex: mainColor)
 }
 }
 }
 
 //MARK:- Properties
 var fieldName = ""
 var dataArray = [AdPostField]()
 var data = [AdPostField]()
 var currentIndex = 0
 var delegateText : textFieldValueDelegate?
 let defaults = UserDefaults.standard
 
 //MARK:- View Life Cycle
 override func awakeFromNib() {
 super.awakeFromNib()
 selectionStyle = .none
 self.setupView()
 }
 
 override func setSelected(_ selected: Bool, animated: Bool) {
 super.setSelected(selected, animated: animated)
 }
 //MARK:- Custom
 func setupView() {
 if defaults.bool(forKey: "isRtl") {
 txtType.textAlignment = .right
 }
 }
 
 //MARK:- IBActions
 @IBAction func textChange(_ sender: HoshiTextField) {
 if let text = sender.text {
 delegateText?.changeText(value: text, fieldTitle: fieldName)
 }
 }
 }
 
 
 class AdPostPopupCell : UITableViewCell, NVActivityIndicatorViewable, SubCategoryDelegate {
 
 //MARK:- Outlets
 @IBOutlet weak var oltPopup: UIButton! {
 didSet {
 oltPopup.setTitleColor(UIColor.darkGray, for: .normal)
 }
 }
 @IBOutlet weak var imgArrow: UIImageView!
 @IBOutlet weak var lblType: UILabel!
 
 //MARK:- Properties
 let storyboard = UIStoryboard(name: "Main", bundle: nil)
 let selectionDropdown = DropDown()
 lazy var dropDowns : [DropDown] = {
 return [
 self.selectionDropdown
 ]
 }()
 var dropDownKeysArray = [String]()
 var dropDownValuesArray = [String]()
 var dropDownImagesArray = [String]()
 var hasCatTemplateArray = [Bool]()
 var hasTempelateArray = [Bool]()
 var hasSubArray = [Bool]()
 var btnPopUpAction: (()->())?
 var selectedValue = String()
 var selectedKey = ""
 var fieldName = ""
 
 var hasCatTempelate = false
 var hasTempelate = false
 var hasSub = false
 
 var dataArray = [AdPostField]()
 var appDel = UIApplication.shared.delegate as! AppDelegate
 var delegatePopup: PopupValueChangeDelegate?
 let defaults = UserDefaults.standard
 
 //MARK:- View Life Cycle
 override func awakeFromNib() {
 super.awakeFromNib()
 selectionStyle = .none
 self.setupView()
 }
 
 //MARK:- Custom
 func setupView() {
 if defaults.bool(forKey: "isRtl") {
 oltPopup.contentHorizontalAlignment = .right
 } else {
 oltPopup.contentHorizontalAlignment = .left
 }
 }
 
 //MARK:- SetUp Drop Down
 func popupShow() {
 selectionDropdown.anchorView = oltPopup
 //   dropDown.anchorView = view // UIView or UIBarButtonItem
 
 // The list of items to display. Can be changed dynamically
 selectionDropdown.dataSource = dropDownValuesArray
 
 /*** IMPORTANT PART FOR CUSTOM CELLS ***/
 selectionDropdown.cellNib = UINib(nibName: "DropDownCellClass", bundle: nil)
 
 selectionDropdown.customCellConfiguration = { (index, item, cell) -> Void in
 guard let cell = cell as? DropDownCellClass else { return }
 
 // Setup your custom UI components
 cell.ImgDropDownCategory.image = UIImage(named: "logo_\(index)")
 }
 
 //        let appearance = DropDown.appearance()
 //
 //        appearance.cellHeight = 60
 //        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
 //        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
 //        //        appearance.separatorColor = UIColor(white: 0.7, alpha: 0.8)
 //      //  appearance.cornerRadius = 10
 //        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
 //        //appearance.shadowOpacity = 0.9
 //       // appearance.shadowRadius = 25
 //        appearance.animationduration = 0.25
 //        appearance.textColor = .darkGray
 //        //        appearance.textFont = UIFont(name: "Georgia", size: 14)
 //
 ////        if #available(iOS 11.0, *) {
 ////            appearance.setupMaskedCorners([.layerMaxXMaxYCorner, .layerMinXMaxYCorner])
 ////        }
 
 
 //    dropDowns.forEach {
 //                    /*** FOR CUSTOM CELLS ***/
 //                    $0.cellNib = UINib(nibName: "CategoryTableViewCell", bundle: nil)
 //
 //                   $0.customCellConfiguration = { (index, item, cell) in
 //                        guard let cell = cell as? CategoryTableViewCell else { return }
 //                        let data = NSData(contentsOf: NSURL(string: self.dropDownImagesArray[index])! as URL)
 //                      cell.imgView.image =  UIImage(data: data! as Data)
 //                       cell.lblCategory.text = self.dropDownValuesArray[index]
 //                        // Setup your custom UI components
 //                        //cell.ImgdropDowncategory.image = UIImage(named: "logo_\(index % 10)")
 //                    }
 //                    /*** ---------------- ***/
 //                }
 //
 
 //        dropDowns.forEach {
 //            /*** FOR CUSTOM CELLS ***/
 //            $0.cellNib = UINib(nibName: "DropDownCellClass", bundle: nil)
 //
 //           $0.customCellConfiguration = { (index, item, cell) in
 //                guard let cell = cell as? DropDownCellClass else { return }
 //                let data = NSData(contentsOf: NSURL(string: self.dropDownImagesArray[index])! as URL)
 //            cell.imageView!.image =  UIImage(data: data! as Data)
 //           // cell.textLabel!.text = self.dropDownValuesArray[index]
 //                // Setup your custom UI components
 //                //cell.ImgdropDowncategory.image = UIImage(named: "logo_\(index % 10)")
 //            }
 //            /*** ---------------- ***/
 //        }
 
 selectionDropdown.selectionAction = { [unowned self]
 (index, item) in
 self.oltPopup.setTitle(item, for: .normal)
 self.selectedValue = item
 self.selectedKey = self.dropDownKeysArray[index]
 self.hasTempelate = self.hasTempelateArray[index]
 self.hasCatTempelate = self.hasCatTemplateArray[index]
 self.hasSub = self.hasSubArray[index]
 self.delegatePopup?.changePopupValue(selectedKey: self.selectedKey, fieldTitle: self.fieldName, selectedText: item)
 
 if self.hasCatTempelate {
 if self.hasTempelate {
 let param: [String: Any] = ["cat_id": self.selectedKey]
 print(param)
 self.adForest_dynamicFields(param: param as NSDictionary)
 }
 }
 
 if self.hasSub {
 let param: [String: Any] = ["subcat": self.selectedKey]
 print(param)
 
 self.adForest_subCategoryData(param: param as NSDictionary)
 }
 }
 }
 
 //MARK:- Delegate Function
 func subCategoryDetails(name: String, id: Int, hasSubType: Bool, hasTempelate: Bool, hasCatTempelate: Bool) {
 print(name, id, hasSubType, hasTempelate, hasCatTempelate)
 if hasSubType {
 let param: [String: Any] = ["subcat": id]
 print(param)
 self.adForest_subCategoryData(param: param as NSDictionary)
 }
 else {
 self.oltPopup.setTitle(name, for: .normal)
 self.selectedKey = String(id)
 self.selectedValue = name
 
 }
 if hasCatTempelate {
 if hasTempelate {
 let param: [String: Any] = ["cat_id" : id]
 print(param)
 self.adForest_dynamicFields(param: param as NSDictionary)
 }
 }
 }
 
 //MARK:- IBActions
 
 @IBAction func actionPopup(_ sender: Any) {
 self.btnPopUpAction?()
 }
 
 //MARK:- API Call
 
 func adForest_dynamicFields(param: NSDictionary) {
 let adPostVC = AadPostController()
 adPostVC.showLoader()
 AddsHandler.adPostDynamicFields(parameter: param, success: { (successResponse) in
 NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
 if successResponse.success {
 AddsHandler.sharedInstance.objAdPostData = successResponse.data.fields
 AddsHandler.sharedInstance.adPostImagesArray = successResponse.data.adImages
 
 }
 else {
 let alert = Constants.showBasicAlert(message: successResponse.message)
 self.appDel.presentController(ShowVC: alert)
 }
 }) { (error) in
 NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
 let alert = Constants.showBasicAlert(message: error.message)
 self.appDel.presentController(ShowVC: alert)
 }
 }
 
 // Sub category data
 func adForest_subCategoryData(param: NSDictionary) {
 let adPostVC = AadPostController()
 adPostVC.showLoader()
 AddsHandler.adPostSubcategory(parameter: param, success: { (successResponse) in
 NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
 if successResponse.success {
 AddsHandler.sharedInstance.objSearchCategory = successResponse.data
 let seacrhCatVC = self.storyboard.instantiateViewController(withIdentifier: "SearchCategoryDetail") as! SearchCategoryDetail
 seacrhCatVC.dataArray = successResponse.data.values
 seacrhCatVC.modalPresentationStyle = .overCurrentContext
 seacrhCatVC.modalTransitionStyle = .crossDissolve
 seacrhCatVC.name = self.selectedValue
 
 seacrhCatVC.delegate = self
 self.appDel.presentController(ShowVC: seacrhCatVC)
 }
 else {
 let alert = Constants.showBasicAlert(message: successResponse.message)
 self.appDel.presentController(ShowVC: alert)
 }
 }) { (error) in
 NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
 let alert = Constants.showBasicAlert(message: error.message)
 self.appDel.presentController(ShowVC: alert)
 }
 }
 }

 
 */
