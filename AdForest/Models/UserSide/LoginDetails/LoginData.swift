//
//  LoginData.swift
//  AdForest
//
//  Created by apple on 3/19/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation

struct LoginData {
    
    var bgColor : String!
    var emailPlaceholder : String!
    var facebookBtn : String!
    var forgotText : String!
    var formBtn : String!
    var googleBtn : String!
    var guestLogin : String!
    var guestText : String!
    var heading : String!
    var logo : String!
    var passwordPlaceholder : String!
    var registerText : String!
    var separator : String!
    var login : String!
    
    var resend :  String!
    var send : String!
    var sms_code : String!
    var mobile_number : String!
    var connect_with_other_ways : String!
    var next : String!
    var password : String!
    
    var isVerifyOn : Bool!
    
    /**
     
     data =     {
     "bg_color" = "#000";
     "connect_with_other_ways" = "Connect with other ways";
     "email_placeholder" = "Your Phone Number";
     "facebook_btn" = Facebook;
     "forgot_text" = "Forgot Password";
     "form_btn" = Submit;
     "google_btn" = "Google+";
     "guest_login" = "Guest Login";
     "guest_text" = Guest;
     heading = "Welcome Back";
     "is_verify_on" = 0;
     logo = "https://thaizzle.com/wp-content/uploads/2017/03/login-banner.jpg";
     "mobile_number" = "Mobile Number";
     next = Next;
     "password_placeholder" = "Your Password";
     "re-send" = "Re-Send";
     "register_text" = "Register with us.";
     send = Send;
     separator = OR;
     "sms_code" = "SMS Code";
     };
     message = "";
     success = 1;
     }
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        bgColor = dictionary["bg_color"] as? String
        emailPlaceholder = dictionary["email_placeholder"] as? String
        facebookBtn = dictionary["facebook_btn"] as? String
        forgotText = dictionary["forgot_text"] as? String
        formBtn = dictionary["form_btn"] as? String
        googleBtn = dictionary["google_btn"] as? String
        guestLogin = dictionary["guest_login"] as? String
        guestText = dictionary["guest_text"] as? String
        heading = dictionary["heading"] as? String
        logo = dictionary["logo"] as? String
        passwordPlaceholder = dictionary["password_placeholder"] as? String
        registerText = dictionary["register_text"] as? String
        separator = dictionary["separator"] as? String
        
         login = dictionary["login"] as? String
         resend = dictionary["re-send"] as? String
         send = dictionary["send"] as? String
         sms_code = dictionary["sms_code"] as? String
         mobile_number = dictionary["mobile_number"] as? String
         connect_with_other_ways = dictionary["connect_with_other_ways"] as? String
         next = dictionary["next"] as? String
          password = dictionary["password"] as? String
        
        isVerifyOn = dictionary["is_verify_on"] as? Bool
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if bgColor != nil{
            dictionary["bg_color"] = bgColor
        }
        if emailPlaceholder != nil{
            dictionary["email_placeholder"] = emailPlaceholder
        }
        if facebookBtn != nil{
            dictionary["facebook_btn"] = facebookBtn
        }
        if forgotText != nil{
            dictionary["forgot_text"] = forgotText
        }
        if formBtn != nil{
            dictionary["form_btn"] = formBtn
        }
        if googleBtn != nil{
            dictionary["google_btn"] = googleBtn
        }
        if guestLogin != nil{
            dictionary["guest_login"] = guestLogin
        }
        if guestText != nil{
            dictionary["guest_text"] = guestText
        }
        if heading != nil{
            dictionary["heading"] = heading
        }
        if logo != nil{
            dictionary["logo"] = logo
        }
        if passwordPlaceholder != nil{
            dictionary["password_placeholder"] = passwordPlaceholder
        }
        if registerText != nil{
            dictionary["register_text"] = registerText
        }
        if separator != nil{
            dictionary["separator"] = separator
        }
        
        if resend != nil{
            dictionary["re-send"] = resend
        }
        
        if send != nil{
            dictionary["send"] = send
        }
        if sms_code != nil{
            dictionary["sms_code"] = sms_code
        }
        if mobile_number != nil{
            dictionary["mobile_number"] = mobile_number
        }
        if connect_with_other_ways != nil{
            dictionary["connect_with_other_ways"] = connect_with_other_ways
        }
        if next != nil{
            dictionary["next"] = next
        }
        if password != nil{
            dictionary["password"] = password
        }
        
        if login != nil{
            dictionary["login"] = login
        }
        if isVerifyOn != nil{
            dictionary["is_verify_on"] = isVerifyOn
        }
        return dictionary
    }
    
}
