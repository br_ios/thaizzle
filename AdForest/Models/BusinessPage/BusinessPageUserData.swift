//
//  BusinessPageUserData.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 08/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation

struct UserData {
    
    var user_id : String!
    var user_address : String!
    var user_image_url : String!
    var user_nicename : String!
    var user_email : String!
    var user_login : String!
    var user_display_name : String!

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        user_id = dictionary["user_id"] as? String
        user_address = dictionary["user_address"] as? String
        user_image_url = dictionary["user_image_url"] as? String
        user_nicename = dictionary["user_nicename"] as? String
        user_email = dictionary["user_email"] as? String
        user_login = dictionary["user_login"] as? String
        user_display_name = dictionary["user_display_name"] as? String
      
        
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if user_id != nil{
            dictionary["user_id"] = user_id
        }
        
        if user_address != nil{
            dictionary["author_id"] = user_address
        }
        
        if user_image_url != nil{
            dictionary["user_image_url"] = user_image_url
        }
        if user_nicename != nil{
            dictionary["user_nicename"] = user_nicename
        }
        
        if user_email != nil{
            dictionary["user_email"] = user_email
        }
        
        if user_login != nil{
            dictionary["user_login"] = user_login
        }
        
        if user_display_name != nil{
            dictionary["user_display_name"] = user_display_name
        }
        return dictionary
    }
    
}
