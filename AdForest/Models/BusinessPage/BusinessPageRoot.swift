//
//  BusinessPageRoot.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 08/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation

struct BusinessPageRoot {
    
    var dataIs : BusinessPageDetail!
    var message : String!
    var success : Bool!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        if let dataData = dictionary["data"] as? [String:Any]{
            dataIs = BusinessPageDetail(fromDictionary: dataData)
        }
        message = dictionary["message"] as? String
        success = dictionary["success"] as? Bool
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if dataIs != nil{
            dictionary["data"] = dataIs.toDictionary()
        }
        if message != nil{
            dictionary["message"] = message
        }
        if success != nil{
            dictionary["success"] = success
        }
        return dictionary
    }
    
}
