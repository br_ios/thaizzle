//
//  BusinessPageDetail.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 08/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation

struct BusinessPageDetail {
    
    var text : String!
    var total_record : Int!
    var total_pages : Int!
    var current_page : String!
    var user : [UserData]!
  
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        
        text = dictionary["text"] as? String
        total_record = dictionary["total_record"] as? Int
        total_pages = dictionary["total_pages"] as? Int
        current_page = dictionary["current_page"] as? String
        
        
        user = [UserData]()
        if let catIconsArray = dictionary["user"] as? [[String:Any]]{
            for dic in catIconsArray{
                let value = UserData(fromDictionary: dic)
                user.append(value)
            }
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if text != nil{
            dictionary["text"] = text
        }
        if total_record != nil{
            dictionary["total_record"] = total_record
        }
        if total_pages != nil{
            dictionary["total_pages"] = total_pages
        }
        if current_page != nil{
            dictionary["current_page"] = current_page
        }
        
        if user != nil{
            var dictionaryElements = [[String:Any]]()
            for catIconsElement in user {
                dictionaryElements.append(catIconsElement.toDictionary())
            }
            dictionary["user"] = dictionaryElements
        }
        
        return dictionary
    }
}
