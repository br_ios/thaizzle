//
//  BusinessShowRoomData.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 06/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation

struct BusinessShowRoomData{
    
    var data : [BusinessShowRoom]!
    var text : String!
    var btn_showroom : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        data = [BusinessShowRoom]()
        if let adsArray = dictionary["data"] as? [[String:Any]]{
            for dic in adsArray{
                let value = BusinessShowRoom(fromDictionary: dic)
                data.append(value)
            }
        }
        text = dictionary["text"] as? String
         btn_showroom = dictionary["btn_showroom"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for adsElement in data {
                dictionaryElements.append(adsElement.toDictionary())
            }
            dictionary["ads"] = dictionaryElements
        }
        if text != nil{
            dictionary["text"] = text
        }
        
        if btn_showroom != nil{
            dictionary["btn_showroom"] = text
        }
        return dictionary
    }
    
}
    
