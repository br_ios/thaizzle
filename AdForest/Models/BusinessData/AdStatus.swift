//
//  AdStatus.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 07/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation

struct AdStatus {
    
    var status : String!
    var status_text : String!
    var featured_type : String!
    var featured_type_text : String!
    
    
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        status = dictionary["status"] as? String
        status_text = dictionary["status_text"] as? String
        featured_type = dictionary["featured_type"] as? String
        featured_type_text = dictionary["featured_type_text"] as? String
        
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if status != nil{
            dictionary["status"] = status
        }
        
        if status_text != nil{
            dictionary["status_text"] = status_text
        }
        
        if featured_type != nil{
            dictionary["featured_type"] = featured_type
        }
        if featured_type_text != nil{
            dictionary["featured_type_text"] = featured_type_text
        }
        
        return dictionary
    }

    
}
