//
//  AdListing.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 07/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation

struct AdListing {
    
    var ad_id : Int!
    var author_id : String!
    var ad_content : String!
    var ad_date_gmt : String!
    var ad_title : String!
    var ad_name : String!
    var ad_parent : Int!
    var ad_guid : String!
    var cat_name : String!
    var ad_date : String!
  //  var ad_price : AdPrice!
    var images : [ProductImageModel]!
   // var ad_video : AdVideo!
    var location : LocationData!
    var ad_status : AdStatus!
    
 
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
         ad_id = dictionary["ad_id"] as? Int
        author_id = dictionary["author_id"] as? String
        ad_content = dictionary["ad_content"] as? String
        ad_date_gmt = dictionary["ad_date_gmt"] as? String
        ad_title = dictionary["ad_title"] as? String
        ad_name = dictionary["ad_name"] as? String
        ad_parent = dictionary["ad_parent"] as? Int
        ad_guid = dictionary["ad_guid"] as? String
        cat_name = dictionary["cat_name"] as? String
        ad_date = dictionary["ad_date"] as? String
        
        if let locationData = dictionary["location"] as? [String:Any]{
            location = LocationData(fromDictionary: locationData)
        }
        if let AdData = dictionary["ad_status"] as? [String:Any]{
            ad_status = AdStatus(fromDictionary: AdData)
        }
        
        images = [ProductImageModel]()
        if let catIconsArray = dictionary["images"] as? [[String:Any]]{
            for dic in catIconsArray{
                let value = ProductImageModel(fromDictionary: dic)
                images.append(value)
            }
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if ad_id != nil{
            dictionary["ad_id"] = ad_id
        }

        if author_id != nil{
            dictionary["author_id"] = author_id
        }

        if ad_content != nil{
            dictionary["ad_content"] = ad_content
        }
        if ad_date_gmt != nil{
            dictionary["ad_date_gmt"] = ad_date_gmt
        }

        if ad_title != nil{
            dictionary["ad_title"] = ad_title
        }

        if ad_name != nil{
            dictionary["ad_name"] = ad_name
        }

        if ad_parent != nil{
            dictionary["ad_parent"] = ad_parent
        }

        if ad_guid != nil{
            dictionary["ad_guid"] = ad_guid
        }

        if cat_name != nil{
            dictionary["cat_name"] = cat_name
        }

        if ad_date != nil{
            dictionary["ad_date"] = ad_date
        }
        
        
        if location != nil{
            dictionary["location"] = location.toDictionary()
        }
        
        if ad_status != nil{
            dictionary["ad_status"] = ad_status.toDictionary()
        }
        
        if images != nil{
            var dictionaryElements = [[String:Any]]()
            for catIconsElement in images {
                dictionaryElements.append(catIconsElement.toDictionary())
            }
            dictionary["images"] = dictionaryElements
        }

        return dictionary
    }
    
}
