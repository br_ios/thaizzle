//
//  LocationData.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 07/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation

struct LocationData {

    var title : String!
    var address : String!
    var lat : String!
    var long : String!

    
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        title = dictionary["title"] as? String
        address = dictionary["address"] as? String
        lat = dictionary["lat"] as? String
        long = dictionary["long"] as? String

    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if title != nil{
            dictionary["title"] = title
        }
        
        if address != nil{
            dictionary["address"] = address
        }
        
        if lat != nil{
            dictionary["lat"] = lat
        }
        if long != nil{
            dictionary["long"] = long
        }
        
        return dictionary
    }

}
