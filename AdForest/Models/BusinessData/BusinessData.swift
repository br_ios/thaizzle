//
//  BusinessData.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 07/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation


struct BusinessRootData {
    
    var dataA : DataDetail!
    var text : String!
    var ad_listing : [AdListing]!

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        if let dataData = dictionary["data"] as? [String:Any]{
            dataA = DataDetail(fromDictionary: dataData)
        }
        text = dictionary["text"] as? String
        
        ad_listing = [AdListing]()
        if let catIconsArray = dictionary["ad_listing"] as? [[String:Any]]{
            for dic in catIconsArray{
                let value = AdListing(fromDictionary: dic)
                ad_listing.append(value)
            }
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
       
        if dataA != nil{
            dictionary["data"] = dataA.toDictionary()
        }
        
        if text != nil{
            dictionary["text"] = text
        }
        
        if ad_listing != nil{
            var dictionaryElements = [[String:Any]]()
            for catIconsElement in ad_listing {
                dictionaryElements.append(catIconsElement.toDictionary())
            }
            dictionary["ad_listing"] = dictionaryElements
        }
        
        return dictionary
    }
}

