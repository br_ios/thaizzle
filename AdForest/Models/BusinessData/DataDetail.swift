//
//  DataDetail.swift
//  AdForest
//
//  Created by Siddharth Awasthi on 07/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation

struct DataDetail {
    
    var nickname : String!
    var first_name : String!
    var last_name : String!
    var email : String!
    var contact : String!
    var address : String!
    var last_login : String!
    var user_image_url : String!
    var display_name : String!
    var posted_by_text : String!
    var user_type : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        
         nickname = dictionary["nickname"] as? String
          first_name = dictionary["first_name"] as? String
          last_name = dictionary["last_name"] as? String
          email = dictionary["email"] as? String
          contact = dictionary["contact"] as? String
          address = dictionary["address"] as? String
        last_login = dictionary["last_login"] as? String
        user_image_url = dictionary["user_image_url"] as? String
          display_name = dictionary["display_name"] as? String
          posted_by_text = dictionary["posted_by_text"] as? String
        user_type = dictionary["user_type"] as? String
        
        
        
        
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if nickname != nil{
            dictionary["nickname"] = nickname
        }
        if first_name != nil{
            dictionary["first_name"] = first_name
        }
        if last_name != nil{
            dictionary["last_name"] = last_name
        }
        if email != nil{
            dictionary["email"] = email
        }
        if contact != nil{
            dictionary["contact"] = contact
        }
        if address != nil{
            dictionary["address"] = address
        }
        if last_login != nil{
            dictionary["last_login"] = last_login
        }
        if user_image_url != nil{
            dictionary["user_image_url"] = user_image_url
        }
        if display_name != nil{
            dictionary["display_name"] = display_name
        }
        if posted_by_text != nil{
            dictionary["posted_by_text"] = posted_by_text
        }
        if user_type != nil{
            dictionary["user_type"] = user_type
        }
        return dictionary
    }
    
}
